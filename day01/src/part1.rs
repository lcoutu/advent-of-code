use regex::Regex;

fn main() {
    let input = include_str!("input1.txt");
    let output = part1(input);
    dbg!(output);
}

fn part1(input: &str) -> String {
    let res: u32 = input.lines().map(extract_number).sum();
    res.to_string()
}

fn extract_number(line: &str) -> u32 {
    let re = Regex::new(r"\d").unwrap();

    let tmp_res: Vec<&str> = re.find_iter(line).map(|m| m.as_str()).collect();
    let res: u32 = [tmp_res[0], tmp_res.last().unwrap()]
        .join("")
        .parse::<u32>()
        .unwrap();

    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = part1(
            "1abc2
        pqr3stu8vwx
        a1b2c3d4e5f
        treb7uchet",
        );
        assert_eq!(result, "142".to_string());
    }
}
