use regex::Regex;

fn main() {
    let input = include_str!("input2.txt");
    let output = solve(input);
    dbg!(output);
}

fn solve(input: &str) -> String {
    let res: u32 = input.lines().map(replace_number).map(extract_number).sum();
    res.to_string()
}

fn replace_number(line: &str) -> String {
    let re = Regex::new(r"one|two|three|four|five|six|seven|eight|nine|zero").unwrap();
    let mut tmp_line: String = line.to_string();
    while re.find(&tmp_line).is_some() {
        let to_replace = re.find(&tmp_line).unwrap().as_str();
        tmp_line = re.replace(&tmp_line, match_number(to_replace)).to_string();
    }
    println!("{} {} {:?} {:?}", { "➤➤➤" }, { "BBB:" }, { line }, {
        tmp_line.clone()
    });
    tmp_line
}

fn match_number(number: &str) -> &str {
    match number {
        "zero" => Some("z0o"),
        "one" => Some("o1e"),
        "two" => Some("t2o"),
        "three" => Some("t3e"),
        "four" => Some("f4r"),
        "five" => Some("f5e"),
        "six" => Some("s6x"),
        "seven" => Some("s7n"),
        "eight" => Some("e8t"),
        "nine" => Some("n9e"),
        _ => None,
    }
    .unwrap()
}

fn extract_number(line: String) -> u32 {
    let re = Regex::new(r"\d").unwrap();

    let tmp_res: Vec<&str> = re.find_iter(&line).map(|m| m.as_str()).collect();
    let res: u32 = [tmp_res[0], tmp_res.last().unwrap()]
        .join("")
        .parse::<u32>()
        .unwrap();
    println!("{} {} {:?} {:?}", { "➤➤➤" }, { "AAA:" }, { line }, {
        res
    });
    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = solve(
            "two1nine
            eightwothree
            abcone2threexyz
            xtwone3four
            4nineeightseven2
            zoneight234
            7pqrstsixteen
            ncqpkzh5twonefxlqbjjhqsrlkhvdnvtbzpcbj",
        );
        assert_eq!(result, "332".to_string());
    }
}
