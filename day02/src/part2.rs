use regex::Regex;
#[macro_use]
extern crate lazy_static;
fn main() {
    let input = include_str!("input1.txt");
    let output = part1(input);
    dbg!(output);
}

#[derive(Debug, Copy, Clone)]
struct ColorSet {
    red: u32,
    blue: u32,
    green: u32,
}

fn part1(input: &str) -> String {
    let res: u32 = input.lines().map(extract_power).sum();
    res.to_string()
}

lazy_static! {
    static ref REG1: Regex = Regex::new(r"^Game (\d+):").unwrap();
    static ref REG2: Regex = Regex::new(r":( .*)$").unwrap();
    static ref REG3: Regex = Regex::new(r"(\d+) (\w+)$").unwrap();
}

fn extract_power(input: &str) -> u32 {
    compute_power(extract_colors(input))
}

fn extract_colors(line: &str) -> Vec<ColorSet> {
    // First extract the part with the colors
    // let re = Regex::new(r":( .*)$").unwrap();

    let color_string = REG2.captures(line).unwrap();
    let color_strings = color_string[1].split(';');

    // let colors_sets: Vec<&str> = color_strings.collect();

    let mut color_sets: Vec<ColorSet> = vec![];
    for set in color_strings {
        let mut tmp_set = ColorSet {
            red: 0,
            green: 0,
            blue: 0,
        };
        for color in set.split(',') {
            // let re = Regex::new(r"(\d+) (\w+)$").unwrap();
            let tmp_cap = REG3.captures(color).unwrap();

            let nb: u32 = tmp_cap[1].parse::<u32>().unwrap();
            match &tmp_cap[2] {
                "red" => tmp_set.red += nb,
                "green" => tmp_set.green += nb,
                "blue" => tmp_set.blue += nb,
                _ => panic!(),
            };
        }
        color_sets.push(tmp_set)
    }

    color_sets
}

fn compute_power(sets: Vec<ColorSet>) -> u32 {
    let mut min_set = ColorSet {
        red: 0,
        blue: 0,
        green: 0,
    };

    for set in sets {
        if set.red > min_set.red {
            min_set.red = set.red
        }
        if set.green > min_set.green {
            min_set.green = set.green
        }
        if set.blue > min_set.blue {
            min_set.blue = set.blue
        }
    }
    min_set.blue * min_set.red * min_set.green
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = part1(
            "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
            Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
            Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
            Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
            Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green",
        );
        assert_eq!(result, "2286".to_string());
    }
}
