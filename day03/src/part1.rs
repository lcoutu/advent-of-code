use regex::Regex;
#[macro_use]
extern crate lazy_static;
fn main() {
    let input = include_str!("input1.txt");
    let output = part1(input);
    dbg!(output);
}

#[derive(Debug, Clone, Default, PartialEq)]
struct NumberPosition {
    line: u32,
    pos: u32,
    is_connected: bool,
}

#[derive(Debug, Copy, Clone)]
struct SymbolPosition {
    line: u32,
    pos: u32,
}

fn part1(input: &str) -> String {
    let (nb_pos, sym_pos): (Vec<NumberPosition>, Vec<SymbolPosition>) =
        extract_nb_n_symbols_pos(input);

    let res: u32 = extract_part_numbers(input, nb_pos, sym_pos);
    res.to_string()
}

lazy_static! {
    static ref REG1: Regex = Regex::new(r"\d").unwrap();
    static ref REG2: Regex = Regex::new(r"[^A-Za-z0-9\.]").unwrap();
}

fn extract_nb_n_symbols_pos(input: &str) -> (Vec<NumberPosition>, Vec<SymbolPosition>) {
    let mut nb_pos: Vec<NumberPosition> = vec![];
    let mut symb_pos: Vec<SymbolPosition> = vec![];

    for (index, line) in input.lines().enumerate() {
        let mut tmp_nb_pos: Vec<NumberPosition> = REG1
            .find_iter(line)
            .map(|m| NumberPosition {
                line: index as u32,
                pos: m.start() as u32,
                is_connected: false,
            })
            .collect();
        nb_pos.append(&mut tmp_nb_pos);
        let mut tmp_symb_pos: Vec<SymbolPosition> = REG2
            .find_iter(line)
            .map(|m| SymbolPosition {
                line: index as u32,
                pos: m.start() as u32,
            })
            .collect();
        symb_pos.append(&mut tmp_symb_pos);
    }
    println!("{} {} {:?}", { "➤➤➤" }, { "DDD:" }, {
        symb_pos.clone()
    });
    (nb_pos, symb_pos)
}

fn is_linked_to_symb(nb_pos: &mut NumberPosition, all_symb: &Vec<SymbolPosition>) {
    for elt in all_symb {
        if elt.line.abs_diff(nb_pos.line) <= 1 && elt.pos.abs_diff(nb_pos.pos) <= 1 {
            nb_pos.is_connected = true
        }
    }
}

fn extract_part_numbers(
    input: &str,
    mut nb_pos: Vec<NumberPosition>,
    sym_pos: Vec<SymbolPosition>,
) -> u32 {
    // Check if connected to symbol, modify it:
    for elt in &mut nb_pos {
        is_linked_to_symb(elt, &sym_pos)
    }

    let mut parts: Vec<u32> = vec![];
    let all_lines: Vec<String> = input.lines().map(String::from).collect();
    let line_length: usize = all_lines[0].len();

    // let mut prev_pos: u32 = 0;
    // let mut prev_line: u32 = 0;

    let mut contig_nb: Vec<&NumberPosition> = vec![];
    // let mut used_nb: Vec<&NumberPosition> = vec![];

    for (i, tmp_nb) in nb_pos.iter().enumerate() {
        let mut tmp_res = get_char(&all_lines, tmp_nb);

        // if used_nb.contains(&tmp_nb) {
        //     continue;
        // } else {
        //     used_nb.push(&tmp_nb);
        // }

        if contig_nb.is_empty() {
            contig_nb.push(tmp_nb);
        } else if nb_pos[i - 1].line == tmp_nb.line && (tmp_nb.pos - nb_pos[i - 1].pos) == 1 {
            contig_nb.push(&tmp_nb)
        } else {
            let mut is_part = false;
            for elt in &contig_nb {
                is_part = is_part || elt.is_connected;
            }

            if is_part {
                parts.push(
                    contig_nb
                        .into_iter()
                        .map(|x| get_char(&all_lines, x))
                        .collect::<Vec<String>>()
                        .concat()
                        .parse::<u32>()
                        .unwrap(),
                );
            }
            contig_nb = vec![tmp_nb];
        }
    }

    if !contig_nb.is_empty() {
        let mut is_part = false;
        for elt in &contig_nb {
            is_part = is_part || elt.is_connected;
        }
        if is_part {
            parts.push(
                contig_nb
                    .into_iter()
                    .map(|x| get_char(&all_lines, x))
                    .collect::<Vec<String>>()
                    .concat()
                    .parse::<u32>()
                    .unwrap(),
            );
        }
    }

    println!("{} {} {:?}", { "➤➤➤" }, { "AAA:" }, { parts.clone() });
    println!("{} {} {:?}", { "➤➤➤" }, { "BBB:" }, {
        nb_pos.clone()
    });
    println!("{} {} {:?}", { "➤➤➤" }, { "CCC:" }, {
        sym_pos.clone()
    });

    parts.into_iter().sum()
}

fn get_char(all_lines: &Vec<String>, nb_pos: &NumberPosition) -> String {
    all_lines[nb_pos.line as usize]
        .chars()
        .nth(nb_pos.pos as usize)
        .unwrap()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = part1(
            "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..",
        );
        assert_eq!(result, "4361".to_string());
    }
}
