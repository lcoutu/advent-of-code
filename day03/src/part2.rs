use itertools::Itertools;
use regex::Regex;
use std::{collections::HashMap, hash::Hash};
#[macro_use]
extern crate lazy_static;
fn main() {
    let input = include_str!("input2.txt");
    let output = part1(input);
    dbg!(output);
}

#[derive(Debug, Clone, Default, PartialEq, Eq)]
struct NumberPosition {
    line: u32,
    pos: u32,
    is_connected: bool,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, Hash)]
struct SymbolPosition {
    line: u32,
    pos: u32,
    conn_parts_nb: u32,
    ratio: u32,
}

fn part1(input: &str) -> String {
    let (nb_pos, sym_pos): (Vec<NumberPosition>, Vec<SymbolPosition>) =
        extract_nb_n_symbols_pos(input);

    let res: u32 = extract_part_numbers(input, nb_pos, sym_pos);
    res.to_string()
}

lazy_static! {
    static ref REG1: Regex = Regex::new(r"\d").unwrap();
    static ref REG2: Regex = Regex::new(r"\*").unwrap();
}

fn extract_nb_n_symbols_pos(input: &str) -> (Vec<NumberPosition>, Vec<SymbolPosition>) {
    let mut nb_pos: Vec<NumberPosition> = vec![];
    let mut symb_pos: Vec<SymbolPosition> = vec![];

    for (index, line) in input.lines().enumerate() {
        let mut tmp_nb_pos: Vec<NumberPosition> = REG1
            .find_iter(line)
            .map(|m| NumberPosition {
                line: index as u32,
                pos: m.start() as u32,
                is_connected: false,
            })
            .collect();
        nb_pos.append(&mut tmp_nb_pos);
        let mut tmp_symb_pos: Vec<SymbolPosition> = REG2
            .find_iter(line)
            .map(|m| SymbolPosition {
                line: index as u32,
                pos: m.start() as u32,
                ..Default::default()
            })
            .collect();
        symb_pos.append(&mut tmp_symb_pos);
    }
    println!("{} {} {:?}", { "➤➤➤" }, { "DDD:" }, {
        symb_pos.clone()
    });
    (nb_pos, symb_pos)
}

fn is_linked_to_symb<'a>(
    nb_pos: &NumberPosition,
    all_symb: &'a Vec<SymbolPosition>,
) -> Vec<&'a SymbolPosition> {
    let mut res: Vec<&SymbolPosition> = vec![];
    for elt in all_symb {
        if elt.line.abs_diff(nb_pos.line) <= 1 && elt.pos.abs_diff(nb_pos.pos) <= 1 {
            res.push(elt)
        }
    }
    res
}

fn extract_part_numbers(
    input: &str,
    mut nb_pos: Vec<NumberPosition>,
    mut sym_pos: Vec<SymbolPosition>,
) -> u32 {
    // Check if connected to symbol, modify it:
    // for elt in &mut nb_pos {
    //     is_linked_to_symb(elt, &mut sym_pos)
    // }

    let mut parts: Vec<(u32, Vec<&SymbolPosition>)> = vec![];
    let all_lines: Vec<String> = input.lines().map(String::from).collect();

    // let mut prev_pos: u32 = 0;
    // let mut prev_line: u32 = 0;

    let mut contig_nb: Vec<&NumberPosition> = vec![];
    let mut is_connected: bool = false;
    let mut connections: Vec<&SymbolPosition> = vec![];
    let mut new_conn: Vec<&SymbolPosition> = vec![];

    // let mut used_nb: Vec<&NumberPosition> = vec![];

    for (i, mut tmp_nb) in nb_pos.iter().enumerate() {
        println!("{} {} {:?}", { "➤➤➤" }, { "III:" }, {
            (get_char(&all_lines, &tmp_nb.clone()), is_connected)
        });
        // new_conn.append(&mut is_linked_to_symb(tmp_nb, &sym_pos));

        println!("{} {} {:?} {:?}", { "➤➤➤" }, { "BBB:" }, { tmp_nb }, {
            new_conn.clone()
        });
        if contig_nb.is_empty() {
            contig_nb.push(tmp_nb);
            new_conn.append(&mut is_linked_to_symb(tmp_nb, &sym_pos));
            if !new_conn.is_empty() {
                is_connected = true
            }
        } else if nb_pos[i - 1].line == tmp_nb.line && (tmp_nb.pos - nb_pos[i - 1].pos) == 1 {
            contig_nb.push(tmp_nb);
            new_conn.append(&mut is_linked_to_symb(tmp_nb, &sym_pos));
            if !new_conn.is_empty() {
                is_connected = true
            }
        }
        // we go to the next number, not contiguous
        else {
            println!(
                "{} {} {:?} {:?}",
                { "➤➤➤" },
                { "AAA:" },
                { tmp_nb.clone() },
                { (is_connected, get_char(&all_lines, &tmp_nb.clone())) }
            );
            if is_connected {
                println!("{} {} {:?}", { "➤➤➤" }, { "EEE:" }, {
                    (contig_nb
                        .clone()
                        .into_iter()
                        .map(|x| get_char(&all_lines, x))
                        .collect::<Vec<String>>(),)
                });
                println!("{} {} {:?}", { "➤➤➤" }, { "FFF:" }, {
                    connections.clone()
                });
                parts.push((
                    contig_nb
                        .into_iter()
                        .map(|x| get_char(&all_lines, x))
                        .collect::<Vec<String>>()
                        .concat()
                        .parse::<u32>()
                        .unwrap(),
                    connections
                        .iter()
                        .unique_by(|&&x| (x.line, x.pos))
                        .copied()
                        .collect(),
                ));
            }
            contig_nb = vec![tmp_nb];
            is_connected = false;
            new_conn = vec![];
            new_conn.append(&mut is_linked_to_symb(tmp_nb, &sym_pos));
            if !new_conn.is_empty() {
                is_connected = true
            }
        }
        connections = new_conn.clone();
        // if !connections.is_empty() {
        //     is_connected = true
        // }
    }

    if !contig_nb.is_empty() {
        let mut is_part = false;
        let mut connections_2: Vec<&SymbolPosition> = vec![];
        let mut is_connected: bool = false;
        for elt in &contig_nb {
            connections_2.append(&mut is_linked_to_symb(elt, &sym_pos));
        }
        if !connections_2.is_empty() {
            is_connected = true
        }
        if is_connected {
            parts.push((
                contig_nb
                    .into_iter()
                    .map(|x| get_char(&all_lines, x))
                    .collect::<Vec<String>>()
                    .concat()
                    .parse::<u32>()
                    .unwrap(),
                connections_2
                    .iter()
                    .unique_by(|&&x| (x.line, x.pos))
                    .copied()
                    .collect(),
            ));
        }
    }

    println!("{} {} {:?}", { "➤➤➤" }, { "AAA:" }, { parts.clone() });

    let mut gears: HashMap<&SymbolPosition, (u32, u32)> = HashMap::new();
    for (part, all_gears) in parts {
        println!("{} {} {:?}", { "➤➤➤" }, { "JJJ:" }, { part });
        println!("{} {} {:?}", { "➤➤➤" }, { "KKK:" }, {
            all_gears.clone()
        });
        for gear in all_gears {
            gears
                .entry(gear)
                .and_modify(|x| x.0 += 1)
                .and_modify(|x| x.1 *= part)
                .or_insert((1, part));
        }
    }

    let mut res: u32 = 0;
    for (key, val) in gears.iter() {
        if val.0 == 2 {
            res += val.1
        }
    }
    // parts.into_iter().sum()
    res
}

fn get_char(all_lines: &Vec<String>, nb_pos: &NumberPosition) -> String {
    all_lines[nb_pos.line as usize]
        .chars()
        .nth(nb_pos.pos as usize)
        .unwrap()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = part1(
            "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..",
        );
        assert_eq!(result, "467835".to_string());
    }
}
