use nom::bytes::complete::take_while1;
use nom::bytes::complete::{tag, take, take_till, take_until, take_until1};
use nom::character::complete::digit1;
use nom::character::is_alphabetic;
use nom::number::complete::u32;
use nom::IResult;
use nom::{
    branch::alt,
    bytes::complete::take_while_m_n,
    bytes::complete::{is_not, take_till1},
    character::complete::char,
    combinator::iterator,
    multi::many_till,
    Parser,
};
use regex::Regex;

use nom::character::complete::space1;
use nom::error::Error;
use nom::multi::separated_list1;

#[macro_use]
extern crate lazy_static;
fn main() {
    println!("{} {} {:?}", { "➤➤➤" }, { "BBB:" }, {});
    let input = include_str!("input1.txt");
    let output = part1(input);
    dbg!(output);
}

#[derive(Default, Debug, Copy, Clone)]
struct ScratchedNum(u32);
#[derive(Default, Debug, Copy, Clone)]
struct WinNum(u32);

fn part1(input: &str) -> String {
    let res = input
        .lines()
        .map(line_parser)
        .map(cards_checker)
        .sum::<u32>();

    println!("{} {} {:?}", { "➤➤➤" }, { "AAA:" }, { res.clone() });
    res.to_string()
}

#[derive(Debug, Clone)]
struct CardNWin<'lt1> {
    scratch_cards: Vec<&'lt1 str>,
    win_num: Vec<&'lt1 str>,
}

fn line_parser(s: &str) -> CardNWin {
    let res: IResult<&str, &str> = take_till(|c| c == ':')(s);

    let res = take::<_, _, Error<_>>(1usize)(res.unwrap().0);
    let res: IResult<_, _> = take_till1(|c: char| c == '|')(res.unwrap().0);
    let (scratch_card, win_numbers) = res.unwrap();
    let win_numbers = win_numbers.trim();
    let scratch_card = scratch_card[1..].trim();

    let scratch_card = separated_list1(space1::<_, Error<_>>, digit1)(scratch_card)
        .unwrap()
        .1;
    let win_numbers = separated_list1(space1::<_, Error<_>>, digit1)(win_numbers)
        .unwrap()
        .1;

    CardNWin {
        scratch_cards: scratch_card,
        win_num: win_numbers,
    }
}

fn cards_checker(input: CardNWin) -> u32 {
    let scratch = input.scratch_cards;
    let win = input.win_num;

    let res = scratch
        .iter()
        .map(|s| match win.contains(s) {
            true => 1,
            false => 0u32,
        })
        .sum();

    match res {
        0 => 0,
        1 => 1,
        _ => u32::pow(2, res - 1),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = part1(
            "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
        );
        assert_eq!(result, "13".to_string());
    }
}
