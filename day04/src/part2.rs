use nom::bytes::complete::take_while1;
use nom::bytes::complete::{tag, take, take_till, take_until, take_until1};
use nom::character::complete::digit1;
use nom::character::complete::space1;
use nom::character::is_alphabetic;
use nom::error::Error;
use nom::multi::separated_list1;
use nom::number::complete::u32;
use nom::IResult;
use nom::{
    branch::alt,
    bytes::complete::take_while_m_n,
    bytes::complete::{is_not, take_till1},
    character::complete::char,
    combinator::iterator,
    multi::many_till,
    Parser,
};
use regex::Regex;
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

#[macro_use]
extern crate lazy_static;
fn main() {
    let input = include_str!("input2.txt");
    let output = part2(input);
    dbg!(output);
}

#[derive(Default, Debug, Copy, Clone)]
struct ScratchedNum(u32);
#[derive(Default, Debug, Copy, Clone)]
struct WinNum(u32);

fn part2(input: &str) -> String {
    let mut all_cards: AllCards = Default::default();

    // Compute all the cards earns and store it into the hashmap:
    for (i, line) in input.lines().enumerate() {
        (_, all_cards) = line_parser(line, i + 1, all_cards);
    }
    for i in (1..all_cards.len() + 1) {
        all_cards = cards_checker(i as usize, all_cards);
    }

    let res: u32 = all_cards.into_iter().map(|x| x.1.copies).sum();
    res.to_string()
}

type AllCards<'lt2> = HashMap<u32, CardNWin<'lt2>>;

#[derive(Debug, Clone)]
struct CardNWin<'lt1> {
    scratch_cards: Vec<&'lt1 str>,
    win_num: Vec<&'lt1 str>,
    copies: u32,
    index: u32,
}

fn line_parser<'a>(s: &'a str, ndx: usize, mut all_cards: AllCards<'a>) -> (usize, AllCards<'a>) {
    let res: IResult<&str, &str> = take_till(|c| c == ':')(s);

    let res = take::<_, _, Error<_>>(1usize)(res.unwrap().0);
    let res: IResult<_, _> = take_till1(|c: char| c == '|')(res.unwrap().0);
    let (scratch_card, win_numbers) = res.unwrap();
    let win_numbers = win_numbers.trim();
    let scratch_card = scratch_card[1..].trim();

    let scratch_card = separated_list1(space1::<_, Error<_>>, digit1)(scratch_card)
        .unwrap()
        .1;
    let win_numbers = separated_list1(space1::<_, Error<_>>, digit1)(win_numbers)
        .unwrap()
        .1;

    let tmp_card = CardNWin {
        scratch_cards: scratch_card,
        win_num: win_numbers,
        copies: 1,
        index: ndx as u32,
    };

    // insert the card in the hashmap:
    all_cards.insert(ndx as u32, tmp_card);
    (ndx, all_cards)
}

fn cards_checker(input_ndx: usize, mut all_cards: AllCards) -> AllCards {
    let tmp_card = all_cards[&(input_ndx as u32).clone()].clone();
    let scratch = &tmp_card.scratch_cards;
    let win = &tmp_card.win_num;

    let mut acc_cards: u32 = 0;

    for elt in scratch {
        match win.contains(elt) {
            true => {
                acc_cards += 1;
                if input_ndx as u32 + acc_cards <= all_cards.len() as u32 + 1 {
                    let tmp_ndx = input_ndx as u32 + acc_cards;
                    let current_card = &all_cards[&(input_ndx as u32)];
                    let mut next_card = all_cards[&tmp_ndx].clone();
                    next_card.copies += current_card.copies;
                    *all_cards.get_mut(&tmp_ndx).unwrap() = next_card.clone();
                }
            }
            false => (),
        }
    }
    all_cards
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = part2(
            "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
        );
        assert_eq!(result, "30".to_string());
    }
}
