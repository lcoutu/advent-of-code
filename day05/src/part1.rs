#![allow(dead_code, unused_imports)]
use std::collections::BTreeMap;

use nom::bytes::complete::{is_a, tag, take, take_till, take_until, take_until1};
use nom::bytes::complete::{take_while, take_while1};
use nom::character::complete::{
    alphanumeric1, anychar, digit1, line_ending, newline, not_line_ending, space0,
};
use nom::character::{is_alphabetic, is_newline};
use nom::number::complete::u32;
use nom::sequence::{pair, preceded, terminated, tuple};
use nom::IResult;
use nom::{
    branch::alt,
    bytes::complete::take_while_m_n,
    bytes::complete::{is_not, take_till1},
    character::complete::char,
    combinator::iterator,
    multi::many_till,
    Parser,
};
use regex::Regex;

use nom::character::complete::space1;
use nom::error::Error;
use nom::multi::{fold_many1, many0, many1, many_m_n, separated_list0, separated_list1};

#[macro_use]
extern crate lazy_static;
fn main() {
    let input = include_str!("input1.txt");
    let output = part1(input);
    dbg!(output);
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
enum Quantity {
    #[default]
    Seed,
    Soil,
    Fertilizer,
    Water,
    Light,
    Temperature,
    Humidity,
    Location,
}

#[derive(Default, Debug, Copy, Clone)]
struct Modifier {
    range: (i64, i64),
    modifier: i64,
}

impl Modifier {
    fn in_range(&self, nb: i64) -> bool {
        assert!(nb > 0);
        nb >= self.range.0 && nb <= self.range.1
    }
}

#[derive(Default, Debug, Clone)]
struct Mapping {
    source: Quantity,
    destination: Quantity,
    modifiers: Vec<Modifier>,
}

impl Mapping {
    // Find a modifier if applicable, otherwise return itself
    fn apply_modifiers(&self, nb: i64) -> i64 {
        for modifier in &self.modifiers {
            if modifier.in_range(nb) {
                return nb + modifier.modifier;
            }
        }
        nb
    }
}

#[derive(Default, Debug, Copy, Clone)]
struct Seed {
    id: i64,
    soil: i64,
}

fn part1(input: &str) -> String {
    let mod_input = [input, "\n\n"].concat();
    let (seeds, mappings): (Vec<Seed>, Vec<Mapping>) = parser(&mod_input);

    let mut res: Vec<Seed> = vec![];
    for seed in seeds.into_iter() {
        let mut tmp_val: i64 = seed.id;
        for mapping in mappings.clone().into_iter() {
            tmp_val = mapping.apply_modifiers(tmp_val)
        }
        res.push(Seed {
            id: seed.id,
            soil: tmp_val,
        })
    }

    let res = res.iter().fold(i64::MAX, |a, &b| a.min(b.soil));
    res.to_string()
}

fn single_group(input: &str) -> IResult<&str, &str> {
    terminated(take_until("\n\n"), tag("\n\n"))(input)
}

fn parse_seeds(input: &str) -> Vec<Seed> {
    let res: IResult<_, _> = preceded(tag("seeds: "), many1(terminated(digit1, space0)))(input);
    let (_, parsed) = res.unwrap();
    let parsed: Vec<Seed> = parsed
        .into_iter()
        .map(|x| x.parse::<i64>().unwrap())
        .map(|x| Seed { id: x, soil: x })
        .collect();
    parsed
}

fn parse_sgl_modifier(input: &str) -> Modifier {
    let res: IResult<_, Vec<_>> = separated_list1(space1, digit1)(input);
    let res = res.unwrap();
    let (dest, src, r) = (
        res.1[0].parse::<i64>().unwrap(),
        res.1[1].parse::<i64>().unwrap(),
        res.1[2].parse::<i64>().unwrap(),
    );

    let md = dest - src;
    let rg = (src, src + r - 1);
    Modifier {
        range: rg,
        modifier: md,
    }
}

fn parse_mapping(input: &str) -> Vec<Modifier> {
    let res: Vec<Modifier> = input.lines().skip(1).map(parse_sgl_modifier).collect();

    res
}

fn parser(input: &str) -> (Vec<Seed>, Vec<Mapping>) {
    let res: IResult<_, _> = many1(single_group)(input);
    let mut res: (&str, Vec<&str>) = res.unwrap();

    let seed_res = parse_seeds(res.1[0]);

    let map_lists: Vec<(Quantity, Quantity)> = vec![
        (Quantity::Seed, Quantity::Soil),
        (Quantity::Soil, Quantity::Fertilizer),
        (Quantity::Fertilizer, Quantity::Water),
        (Quantity::Water, Quantity::Light),
        (Quantity::Light, Quantity::Temperature),
        (Quantity::Temperature, Quantity::Humidity),
        (Quantity::Humidity, Quantity::Location),
    ];

    let parsed_mappings: Vec<&str> = res.1.drain(1..).collect();
    let mut mappings: Vec<Mapping> = vec![];
    for (string, (src, dest)) in parsed_mappings.into_iter().zip(map_lists.into_iter()) {
        let tmp_mapping = Mapping {
            source: src,
            destination: dest,
            modifiers: parse_mapping(string),
        };
        mappings.push(tmp_mapping);
    }

    (seed_res, mappings)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        let result = part1(
            "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4",
        );
        assert_eq!(result, "35".to_string());
    }
}
