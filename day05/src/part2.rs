#![allow(dead_code, unused_imports)]
use nom::bytes::complete::{is_a, tag, take, take_till, take_until, take_until1};
use nom::bytes::complete::{take_while, take_while1};
use nom::character::complete::{
    alphanumeric1, anychar, digit1, line_ending, newline, not_line_ending, space0,
};
use nom::character::{is_alphabetic, is_newline};
use nom::number::complete::u32;
use nom::sequence::{pair, preceded, terminated, tuple};
use nom::IResult;
use nom::{
    branch::alt,
    bytes::complete::take_while_m_n,
    bytes::complete::{is_not, take_till1},
    character::complete::char,
    combinator::iterator,
    multi::many_till,
    Parser,
};
use rayon::prelude::*;
use regex::Regex;
use std::collections::BTreeMap;
use std::time::Instant;

use nom::character::complete::space1;
use nom::error::Error;
use nom::multi::{fold_many1, many0, many1, many_m_n, separated_list0, separated_list1};

#[macro_use]
extern crate lazy_static;
fn main() {
    let input = include_str!("input1.txt");

    let now = Instant::now();
    let output = part1(input);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92mExecute in:\x1b[0m" },
        { elapsed }
    );
    dbg!(output);
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
enum Quantity {
    #[default]
    Seed,
    Soil,
    Fertilizer,
    Water,
    Light,
    Temperature,
    Humidity,
    Location,
}

#[derive(Default, Debug, Copy, Clone)]
struct Modifier {
    range: (i64, i64),
    modifier: i64,
}

impl Modifier {
    fn in_range(&self, nb: i64) -> bool {
        // assert!(nb > 0);
        nb >= self.range.0 && nb <= self.range.1
    }
}

#[derive(Default, Debug, Clone)]
struct Mapping {
    source: Quantity,
    destination: Quantity,
    modifiers: Vec<Modifier>,
}

impl Mapping {
    // Find a modifier if applicable, otherwise return itself
    fn apply_modifiers(&self, nb: i64) -> i64 {
        for modifier in &self.modifiers {
            if modifier.in_range(nb) {
                return nb + modifier.modifier;
            }
        }
        nb
    }
}

#[derive(Default, Debug, Copy, Clone)]
struct Seed {
    id: i64,
    soil: i64,
}

fn part1(input: &str) -> String {
    let mod_input = [input, "\n\n"].concat();
    // let (seeds, mappings): (Vec<Seed>, Vec<Mapping>) = parser(&mod_input);

    // let mut res: Vec<Seed> = vec![];
    // for seed in seeds.into_iter() {
    //     let mut tmp_val: i64 = seed.id;
    //     for mapping in mappings.clone().into_iter() {
    //         tmp_val = mapping.apply_modifiers(tmp_val)
    //     }
    //     res.push(Seed {
    //         id: seed.id,
    //         soil: tmp_val,
    //     })
    // }

    // let res = res.iter().fold(i64::MAX, |a, &b| a.min(b.soil));
    let res = parser(&mod_input);
    res.to_string()
}

fn single_group(input: &str) -> IResult<&str, &str> {
    terminated(take_until("\n\n"), tag("\n\n"))(input)
}

fn process_seeds(input: &str, mappings: Vec<Mapping>) -> i64 {
    let res: IResult<_, _> = preceded(tag("seeds: "), many1(terminated(digit1, space0)))(input);

    let (_, parsed) = res.unwrap();
    assert!(parsed.len() % 2 == 0, "Should be even number of seeds");

    let l = parsed.len();
    let mut min_loc: i64 = i64::MAX;
    let mut par_min: Vec<i64> = vec![];
    for i in 0..(l / 2) {
        println!(
            "{} {} {:?}",
            { "\x1b[38;5;92m➤➤➤\x1b[0m" },
            { "\x1b[38;5;92mPair:\x1b[0m" },
            { (i, l / 2) }
        );
        let s_0 = parsed[2 * i].parse::<i64>().unwrap();
        let range = parsed[2 * i + 1].parse::<i64>().unwrap();
        let seed_range = s_0..(s_0 + range);

        let tmp_res = seed_range
            .into_par_iter()
            .map(|x| {
                let mut tmp_res = x;
                for mapping in &mappings {
                    tmp_res = mapping.apply_modifiers(tmp_res)
                }
                tmp_res
            })
            .min()
            .unwrap();
        par_min.push(tmp_res);
    }

    *par_min.iter().min().unwrap()
}

fn parse_sgl_modifier(input: &str) -> Modifier {
    let res: IResult<_, Vec<_>> = separated_list1(space1, digit1)(input);
    let res = res.unwrap();
    let (dest, src, r) = (
        res.1[0].parse::<i64>().unwrap(),
        res.1[1].parse::<i64>().unwrap(),
        res.1[2].parse::<i64>().unwrap(),
    );

    let md = dest - src;
    let rg = (src, src + r - 1);
    Modifier {
        range: rg,
        modifier: md,
    }
}

fn parse_mapping(input: &str) -> Vec<Modifier> {
    let res: Vec<Modifier> = input.lines().skip(1).map(parse_sgl_modifier).collect();

    res
}

fn parser(input: &str) -> i64 {
    let res: IResult<_, _> = many1(single_group)(input);
    let mut res: (&str, Vec<&str>) = res.unwrap();

    let map_lists: Vec<(Quantity, Quantity)> = vec![
        (Quantity::Seed, Quantity::Soil),
        (Quantity::Soil, Quantity::Fertilizer),
        (Quantity::Fertilizer, Quantity::Water),
        (Quantity::Water, Quantity::Light),
        (Quantity::Light, Quantity::Temperature),
        (Quantity::Temperature, Quantity::Humidity),
        (Quantity::Humidity, Quantity::Location),
    ];

    let parsed_mappings: Vec<&str> = res.1.drain(1..).collect();
    let mut mappings: Vec<Mapping> = vec![];
    for (string, (src, dest)) in parsed_mappings.into_iter().zip(map_lists.into_iter()) {
        let tmp_mapping = Mapping {
            source: src,
            destination: dest,
            modifiers: parse_mapping(string),
        };
        mappings.push(tmp_mapping);
    }

    let seed_res = process_seeds(res.1[0], mappings);

    seed_res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        let result = part1(
            "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4",
        );
        assert_eq!(result, "46".to_string());
    }
}
