#![allow(dead_code, unused_imports)]
use core::panic;
use std::collections::BTreeMap;

use nom::bytes::complete::{is_a, tag, take, take_till, take_until, take_until1};
use nom::bytes::complete::{take_while, take_while1};
use nom::character::complete::{
    alphanumeric1, anychar, digit1, line_ending, newline, not_line_ending, space0,
};
use nom::character::{is_alphabetic, is_newline};
use nom::number::complete::u64;
use nom::sequence::{pair, preceded, terminated, tuple};
use nom::IResult;
use nom::{
    branch::alt,
    bytes::complete::take_while_m_n,
    bytes::complete::{is_not, take_till1},
    character::complete::char,
    combinator::iterator,
    multi::many_till,
    Parser,
};
use regex::Regex;

use nom::character::complete::space1;
use nom::error::Error;
use nom::multi::{fold_many1, many0, many1, many_m_n, separated_list0, separated_list1};

#[macro_use]
extern crate lazy_static;
fn main() {
    // let input = include_str!("input1.txt");
    let input = vec![
        Race {
            time: 56977793,
            dist: 499221010971440,
        },
        // Race {
        //     time: 97,
        //     dist: 2210,
        // },
        // Race {
        //     time: 77,
        //     dist: 1097,
        // },
        // Race {
        //     time: 93,
        //     dist: 1440,
        // },
    ];
    let output = part1(input);
    dbg!(output);
}

#[derive(Default, Debug, Copy, Clone)]
struct Race {
    time: u64,
    dist: u64,
}

impl Race {
    fn compute_win_times_nb(&self) -> Vec<u64> {
        let discr = self.discriminant();
        let sols: Vec<u64> = if discr > 0 {
            self.pos_delta_solutions(discr)
        } else if discr == 0 {
            self.delta_zero_solution()
        } else {
            panic!("There should be a solution");
        };
        sols
    }

    fn discriminant(&self) -> u64 {
        let discr = u64::checked_sub(u64::pow(self.time, 2), 4 * self.dist);

        if let Some(res) = discr {
            res
        } else {
            panic!("Expect to have at least 1 solution")
        }
    }

    fn delta_zero_solution(&self) -> Vec<u64> {
        assert!(self.time % 2 != 0);
        vec![self.time / (2 * 1)]
    }

    fn pos_delta_solutions(&self, delta: u64) -> Vec<u64> {
        let sol_1: f64 = (self.time as f64 - (delta as f64).sqrt()) / (2.);
        let sol_2: f64 = (self.time as f64 + (delta as f64).sqrt()) / (2.);

        let sol_2: u64 = if sol_2.trunc() == sol_2 {
            sol_2 as u64 - 1
        } else {
            sol_2.trunc() as u64
        };
        let sol_1: u64 = if sol_1.trunc() == sol_1 {
            sol_1 as u64 + 1
        } else {
            sol_1.trunc() as u64 + 1
        };
        (sol_1..sol_2 + 1).collect()
    }
}

fn part1(races: Vec<Race>) -> String {
    let res = races
        .iter()
        .fold(1u64, |acc, x| acc * x.compute_win_times_nb().len() as u64);
    res.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        let input = vec![
            Race {
                time: 71530,
                dist: 940200,
            },
            // Race { time: 15, dist: 40 },
            // Race {
            //     time: 30,
            //     dist: 200,
            // },
        ];
        let result = part1(input);
        assert_eq!(result, "71503".to_string());
    }
}
