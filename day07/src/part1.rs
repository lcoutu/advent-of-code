#![allow(dead_code, unused_imports)]
use nom::bytes::complete::{is_a, tag, take, take_till, take_until, take_until1};
use nom::bytes::complete::{take_while, take_while1};
use nom::character::complete::{
    alphanumeric1, anychar, digit1, line_ending, newline, not_line_ending, space0,
};
use nom::character::{is_alphabetic, is_newline};
use nom::error::Error;
use nom::number::complete::u32;
use nom::sequence::{pair, preceded, separated_pair, terminated, tuple};
use nom::{
    branch::alt,
    bytes::complete::take_while_m_n,
    bytes::complete::{is_not, take_till1},
    character::complete::char,
    combinator::iterator,
    multi::many_till,
    Parser,
};
use nom::{Err, IResult, InputIter};
use regex::Regex;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::str::FromStr;
use std::time::Instant;

use nom::character::complete::space1;

use nom::multi::{
    fold_many0, fold_many1, many0, many1, many_m_n, separated_list0, separated_list1,
};

#[macro_use]
extern crate lazy_static;
use enum_iterator::{all, cardinality, first, last, next, previous, reverse_all, Sequence};
use nom_supreme::ParserExt;
use std::cmp::Ordering;
use strum_macros::EnumString;

fn main() {
    let input = include_str!("input1.txt");
    let now = Instant::now();
    let output = part1(input);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92mExecute in:\x1b[0m" },
        { elapsed }
    );
    dbg!(output);
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, EnumString, Hash)]
enum Card {
    C2,
    C3,
    C4,
    C5,
    C6,
    C7,
    C8,
    C9,
    T,
    J,
    Q,
    K,
    A,
}

impl Card {
    fn value(&self) -> u32 {
        match *self {
            Card::C2 => 1,
            Card::C3 => 2,
            Card::C4 => 3,
            Card::C5 => 4,
            Card::C6 => 5,
            Card::C7 => 6,
            Card::C8 => 7,
            Card::C9 => 8,
            Card::T => 9,
            Card::J => 10,
            Card::Q => 11,
            Card::K => 12,
            Card::A => 13,
        }
    }
}

#[derive(Debug, Copy, Clone)]
struct Hand {
    cards: [Card; 5],
    val: u32,
}

impl Hand {
    fn greater_than(&self, other: &Self) -> bool {
        // self.compute_rank() > other.compute_rank()

        let a = self.compute_rank();
        let b = other.compute_rank();
        if a != b {
            return a > b;
        } else {
            for i in 0..5 {
                if self.cards[i as usize].value() != other.cards[i as usize].value() {
                    return self.cards[i as usize].value() > other.cards[i as usize].value();
                }
            }
        }
        false
    }

    fn compute_rank(&self) -> u32 {
        // let res = HashSet::from(self.cards);
        let mut map = HashMap::new();
        for item in self.cards {
            *map.entry(item).or_insert(0) += 1;
        }

        let mut res = 0;

        for (i, key_val) in map.iter().enumerate() {
            // let tmp_val = *tmp_card.;
            let (tmp_card, cnt) = key_val;
            res += match cnt {
                5 => 4000000 * 5,
                4 => 3000000 * 4,
                3 => 2000000 * 3,
                2 => 1000000 * 2,
                1 => 1,
                _ => 0,
            };
        }

        res
    }

    fn from_line(cards: &str, val: u32) -> Hand {
        let cards = many0(take::<_, _, Error<_>>(1usize)).parse(cards);

        let cards: Vec<Card> = cards
            .unwrap()
            .1
            .iter()
            .map(|&x| match x {
                "2" => Card::C2,
                "3" => Card::C3,
                "4" => Card::C4,
                "5" => Card::C5,
                "6" => Card::C6,
                "7" => Card::C7,
                "8" => Card::C8,
                "9" => Card::C9,
                "T" => Card::T,
                "J" => Card::J,
                "Q" => Card::Q,
                "K" => Card::K,
                "A" => Card::A,
                _ => panic!("not a card"),
            })
            .collect::<Vec<Card>>();

        let tmp_cards: [Card; 5] = cards.try_into().unwrap();
        Hand {
            cards: tmp_cards,
            val,
        }
    }
}

fn part1(input: &str) -> String {
    let mut parsed: Vec<Hand> = parser(input);
    parsed.sort_unstable_by(|a, b| {
        if a.greater_than(b) {
            Ordering::Greater
        } else {
            Ordering::Less
        }
    });
    let res: u32 = parsed
        .iter()
        .enumerate()
        .map(|(n, x)| (n as u32 + 1) * x.val)
        .sum();
    res.to_string()
}

fn parse_line(line: &str) -> Hand {
    let res = separated_pair(alphanumeric1::<_, Error<_>>, tag(" "), digit1)(line)
        .unwrap()
        .1;
    let res: Hand = Hand::from_line(res.0, res.1.parse::<u32>().unwrap());
    res
}

fn parser(input: &str) -> Vec<Hand> {
    let mut res: Vec<Hand> = input.lines().map(parse_line).collect();
    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        let result = part1(
            "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483",
        );
        assert_eq!(result, "6440".to_string());
    }
}
