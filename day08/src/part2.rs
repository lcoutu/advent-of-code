#![allow(dead_code, unused_imports)]
use nom::bytes::complete::{is_a, tag, take, take_till, take_until, take_until1};
use nom::bytes::complete::{take_while, take_while1};
use nom::character::complete::{
    alpha0, alphanumeric0, alphanumeric1, anychar, digit1, line_ending, newline, not_line_ending,
    space0,
};
use nom::character::{is_alphabetic, is_newline};
use nom::error::Error;
use nom::number::complete::u32;
use nom::sequence::{delimited, pair, preceded, separated_pair, terminated, tuple};
use nom::{
    branch::alt,
    bytes::complete::take_while_m_n,
    bytes::complete::{is_not, take_till1},
    character::complete::char,
    combinator::iterator,
    multi::many_till,
    Parser,
};
use nom::{Err, IResult, InputIter};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use regex::Regex;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::num;
use std::str::FromStr;
use std::time::Instant;

use nom::character::complete::space1;

use nom::multi::{
    fold_many0, fold_many1, many0, many1, many_m_n, separated_list0, separated_list1,
};

#[macro_use]
extern crate lazy_static;
use enum_iterator::{all, cardinality, first, last, next, previous, reverse_all, Sequence};
use nom_supreme::ParserExt;
use std::cmp::Ordering;
use strum_macros::EnumString;

fn main() {
    let input = include_str!("input1.txt");
    let now = Instant::now();
    let output = part1(input);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92mExecute in:\x1b[0m" },
        { elapsed }
    );
    dbg!(output);
}

fn part1(input: &str) -> String {
    let (full_map, instrs) = parser(input);

    let mut map_input: Vec<MapInput<'_>> = full_map.keys().fold(vec![], |mut acc, x| {
        if x.ends_with('A') {
            acc.push(full_map[x])
        };
        acc
    });
    map_input.iter_mut().enumerate().for_each(|(i, x)| {
        x.ndx = i;
    });

    let mut res = 0;
    let mut thread_end: BTreeMap<usize, usize> = BTreeMap::new();

    while true {
        let (tmp_map, is_arrived) = instrs.play_instr(res, map_input, &full_map);

        res += 1;
        map_input = tmp_map
            .iter()
            .enumerate()
            .filter_map(|(i, x)| -> Option<MapInput<'_>> {
                if is_arrived[i] {
                    thread_end.insert(x.ndx, res);
                    None
                } else {
                    Some(*x)
                }
            })
            .collect();

        if map_input.is_empty() {
            break;
        }
    }

    let total_product: usize = thread_end.values().product::<usize>();

    let mut res: usize = thread_end[&0];
    for i in 1..(thread_end.len()) {
        res = lcm(res, thread_end[&i]);
    }
    res.to_string()
}

fn lcm(first: usize, second: usize) -> usize {
    first * second / gcd(first, second)
}

fn gcd(mut a: usize, mut b: usize) -> usize {
    while b != 0 {
        let remainder = a % b;
        a = b;
        b = remainder;
    }
    a
}

#[derive(Debug, Copy, Clone, EnumString)]
enum Direction {
    R,
    L,
    // U,
    // D,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum IsArrived {
    Arrived,
    Continue,
}

#[derive(Default, Debug, Clone)]
struct Instructions {
    dirs: Vec<Direction>,
}

impl Instructions {
    fn play_instr<'a>(
        &'a self,
        ndx: usize,
        map_inputs: Vec<MapInput>,
        full_map: &'a FullMap,
    ) -> (Vec<MapInput>, Vec<bool>) {
        let tmp_instr = self.dirs[ndx % self.dirs.len()];

        let out_maps: Vec<MapInput> = map_inputs
            // .iter()
            .par_iter()
            .map(|x| {
                let mut tmp_map = *full_map.get(x.entry).unwrap();
                tmp_map.ndx = x.ndx;
                tmp_map
            })
            .map(|x| match tmp_instr {
                Direction::R => (x.ndx, x.right_out),
                Direction::L => (x.ndx, x.left_out),
            })
            .map(|(ndx, x)| {
                let mut tmp_map = full_map[x];
                tmp_map.ndx = ndx;
                tmp_map
            })
            .collect();

        let exit: Vec<bool> = out_maps.iter().map(|x| x.entry.ends_with('Z')).collect();

        (out_maps, exit)
    }
}

#[derive(Default, Debug, Copy, Clone)]
struct MapInput<'a> {
    entry: &'a str,
    left_out: &'a str,
    right_out: &'a str,
    ndx: usize,
}

type FullMap<'a> = BTreeMap<&'a str, MapInput<'a>>;

fn parser(input: &str) -> (FullMap, Instructions) {
    let mut parser_1 = alphanumeric0::<&str, Error<_>>.terminated(tag("\n\n"));

    let (map_lines, dir_str): (&str, &str) = parser_1.parse(input).unwrap();

    let dirs = many0(take::<_, _, Error<_>>(1usize)).parse(dir_str);
    let dirs: Vec<Direction> = dirs
        .unwrap()
        .1
        .iter()
        .map(|&x| match x {
            "R" => Direction::R,
            "L" => Direction::L,
            _ => panic!("expect a direction"),
        })
        .collect::<Vec<Direction>>();

    let mut parse_line = separated_pair(
        alphanumeric0::<&str, Error<_>>,
        tag(" = "),
        delimited(
            tag("("),
            separated_pair(alphanumeric0, tag(", "), alphanumeric0),
            tag(")"),
        ),
    );

    let mut full_map: FullMap = BTreeMap::new();
    map_lines.lines().for_each(|x| {
        let (_, (elt_1, (elt_2, elt_3))) = parse_line.parse(x).unwrap();
        let elt_input = MapInput {
            entry: elt_1,
            left_out: elt_2,
            right_out: elt_3,
            ndx: 0,
        };
        full_map.insert(elt_1, elt_input);
    });
    (full_map, Instructions { dirs })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        let result = part1(
            "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)",
        );
        assert_eq!(result, "6".to_string());
    }
}
