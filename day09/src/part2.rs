#![allow(dead_code, unused_imports)]

use itertools::Itertools;
use nom::character::complete::{digit0, digit1, newline, space1};
use nom::combinator::opt;
use nom::error::Error;
use nom::multi::{many1, separated_list1};
use nom::sequence::{terminated, tuple};
use nom::{IResult, Parser};
use nom_supreme::{tag::complete::tag, ParserExt};
use regex::Regex;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::iter::zip;
use std::str::FromStr;
use std::time::Instant;

#[macro_use]
extern crate lazy_static;
use enum_iterator::{all, cardinality, first, last, next, previous, reverse_all, Sequence};
use std::cmp::Ordering;
use strum_macros::EnumString;

fn main() {
    let input = include_str!("input1.txt");
    let now = Instant::now();
    let output = part1(input);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92mExecute in:\x1b[0m" },
        { elapsed }
    );
    dbg!(output);
}

fn part1(input: &str) -> String {
    let mut res: History = parser(input);

    let res: i32 = res.iter_mut().map(|x| x.extrapolate()).sum();

    res.to_string()
}

type History = Vec<OasisParam>;

#[derive(Default, Debug, Clone)]
struct OasisParam(Vec<i32>);

impl OasisParam {
    fn extra_steps(&mut self) -> OasisParam {
        OasisParam(
            zip(self.0.iter().rev().skip(1).rev(), self.0.iter().skip(1))
                .map(|(a, b)| a - b)
                .collect(),
        )
    }

    fn extrapolate(&mut self) -> i32 {
        let mut tmp_params = vec![self.clone(), self.extra_steps()];
        let mut first_elts = vec![self.first_elt(), tmp_params.last().unwrap().first_elt()];

        while !tmp_params.last().unwrap().is_null() {
            let a = tmp_params.last().unwrap().clone().extra_steps();
            first_elts.push(a.first_elt());
            tmp_params.push(a)
        }
        let mut seq_diff: i32 = 0;
        for elt in first_elts.iter().rev() {
            seq_diff += elt;
        }

        let res: i32 = seq_diff;
        res
    }

    fn is_null(&self) -> bool {
        self.0.iter().all(|x| *x == 0)
    }

    fn first_elt(&self) -> i32 {
        *self.0.first().unwrap()
    }
}

fn parse_neg_num(input: &str) -> IResult<&str, i32> {
    let (input, nb) = tuple((opt(tag::<&str, &str, Error<_>>("-")), digit1))(input).unwrap();

    let nb = match nb.0 {
        Some(_) => -nb.1.parse::<i32>().unwrap(),
        None => nb.1.parse::<i32>().unwrap(),
    };

    Ok((input, nb))
}

fn parser(input: &str) -> History {
    let parsed: History = input.lines().fold(vec![], |mut acc, line| {
        let (_, nbs) = separated_list1(space1::<_, Error<_>>, parse_neg_num)(line).unwrap();

        acc.push(OasisParam(nbs));
        acc
    });
    parsed
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        let result = part1(
            "0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45",
        );
        assert_eq!(result, "2".to_string());
    }
}
