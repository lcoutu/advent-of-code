#![allow(dead_code, unused_imports)]

use glam::IVec2;
use itertools::Itertools;
use nom::branch::alt;
use nom::bytes::complete::{is_a, is_not, tag, take_till1, take_until};
use nom::character::complete::{digit0, digit1, line_ending, newline, space1};
use nom::combinator::{iterator, opt, value};
use nom::complete::take;
use nom::error::{make_error, Error, ErrorKind};
use nom::multi::{many0, many1, separated_list1};
use nom::sequence::{terminated, tuple};
use nom::{Err, Finish, IResult, Parser};
use nom_locate::{position, LocatedSpan};
// use nom_supreme::{tag::complete::tag, ParserExt};
use regex::Regex;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::iter::zip;
use std::str::FromStr;
use std::time::Instant;
#[macro_use]
extern crate lazy_static;
use enum_iterator::{all, cardinality, first, last, next, previous, reverse_all, Sequence};
use std::cmp::Ordering;
use strum_macros::EnumString;

fn main() {
    let input = include_str!("input1.txt");
    let now = Instant::now();
    let output = part1(input);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92mExecute in:\x1b[0m" },
        { elapsed }
    );
    dbg!(output);
}

fn part1(input: &str) -> String {
    let mut tilemap = parser(input);

    tilemap.find_start();
    tilemap.find_path();
    let path_1 = tilemap.travel_path(1);
    // let path_2 = tilemap.travel_path(2);

    let res = path_1 / 2;
    res.to_string()
}

type Span<'a> = LocatedSpan<&'a str>;
type SpanIVec2<'a> = LocatedSpan<&'a str, IVec2>;

#[derive(Debug, Copy, Clone, Default, PartialEq)]
enum TileContent {
    #[default]
    Empty,
    NtoE,
    NtoW,
    StoE,
    StoW,
    Start,
    HorPipe,
    VertPipe,
    NewLine,
}

#[derive(Default, Debug, PartialEq, Eq, Clone, Copy)]
enum Dir {
    #[default]
    Up,
    Down,
    Left,
    Right,
}

fn with_xy(span: Span) -> SpanIVec2 {
    // column/location are 1-indexed
    let x = span.get_column() as i32 - 1;
    let y = span.location_line() as i32 - 1;
    span.map_extra(|_| IVec2::new(x, y))
}

#[derive(Default, Debug, Copy, Clone, PartialEq)]
struct Meter {
    pos: IVec2,
    length: u32,
    dir: Dir,
    c: TileContent,
}

impl Meter {
    fn add(&self, next_tile: TileContent) -> Meter {
        match self.dir {
            Dir::Up => match next_tile {
                TileContent::StoE => Meter {
                    pos: self.pos + IVec2::Y,
                    length: self.length + 1,
                    dir: Dir::Right,
                    c: next_tile,
                },
                TileContent::StoW => Meter {
                    pos: self.pos + IVec2::Y,
                    length: self.length + 1,
                    dir: Dir::Left,
                    c: next_tile,
                },
                TileContent::VertPipe => Meter {
                    pos: self.pos + IVec2::Y,
                    length: self.length + 1,
                    dir: Dir::Up,
                    c: next_tile,
                },
                _ => panic!(),
            },

            Dir::Down => match next_tile {
                TileContent::NtoE => Meter {
                    pos: self.pos + IVec2::NEG_Y,
                    length: self.length + 1,
                    dir: Dir::Right,
                    c: next_tile,
                },
                TileContent::NtoW => Meter {
                    pos: self.pos + IVec2::NEG_Y,
                    length: self.length + 1,
                    dir: Dir::Left,
                    c: next_tile,
                },
                TileContent::VertPipe => Meter {
                    pos: self.pos + IVec2::NEG_Y,
                    length: self.length + 1,
                    dir: Dir::Down,
                    c: next_tile,
                },
                _ => panic!(),
            },
            Dir::Left => match next_tile {
                TileContent::NtoE => Meter {
                    pos: self.pos + IVec2::NEG_X,
                    length: self.length + 1,
                    dir: Dir::Up,
                    c: next_tile,
                },
                TileContent::StoE => Meter {
                    pos: self.pos + IVec2::NEG_X,
                    length: self.length + 1,
                    dir: Dir::Down,
                    c: next_tile,
                },
                TileContent::HorPipe => Meter {
                    pos: self.pos + IVec2::NEG_X,
                    length: self.length + 1,
                    dir: Dir::Left,
                    c: next_tile,
                },
                _ => panic!(),
            },
            Dir::Right => match next_tile {
                TileContent::NtoW => Meter {
                    pos: self.pos + IVec2::X,
                    length: self.length + 1,
                    dir: Dir::Up,
                    c: next_tile,
                },
                TileContent::StoW => Meter {
                    pos: self.pos + IVec2::X,
                    length: self.length + 1,
                    dir: Dir::Down,
                    c: next_tile,
                },
                TileContent::HorPipe => Meter {
                    pos: self.pos + IVec2::X,
                    length: self.length + 1,
                    dir: Dir::Right,
                    c: next_tile,
                },
                _ => panic!(),
            },
        }
    }
}

#[derive(Default, Debug, Clone)]
struct Map {
    tiles: HashMap<IVec2, TileContent>,
    start: IVec2,
    path_1: Meter,
    path_2: Meter,
}

impl Map {
    fn find_start(&mut self) {
        let res = self
            .tiles
            .iter()
            .find(|(_, val)| matches!(**val, TileContent::Start));

        self.start = *res.unwrap().0;
    }

    fn find_path(&mut self) {
        if let Some(x) = self.tiles.get(&(self.start + IVec2::NEG_X)) {
            match x {
                TileContent::NtoE => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::NEG_X,
                        length: 1,
                        dir: Dir::Left,
                        c: TileContent::NtoE,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                TileContent::StoE => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::NEG_X,
                        length: 1,
                        dir: Dir::Left,
                        c: TileContent::StoE,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                TileContent::HorPipe => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::NEG_X,
                        length: 1,
                        dir: Dir::Left,
                        c: TileContent::HorPipe,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                _ => (),
            }
        }

        if let Some(x) = self.tiles.get(&(self.start + IVec2::NEG_Y)) {
            match x {
                TileContent::NtoE => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::NEG_Y,
                        length: 1,
                        dir: Dir::Right,
                        c: TileContent::NtoE,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                TileContent::NtoW => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::NEG_Y,
                        length: 1,
                        dir: Dir::Left,
                        c: TileContent::NtoW,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                TileContent::VertPipe => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::NEG_Y,
                        length: 1,
                        dir: Dir::Down,
                        c: TileContent::VertPipe,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                _ => (),
            }
        }

        if let Some(x) = self.tiles.get(&(self.start + IVec2::X)) {
            match x {
                TileContent::NtoW => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::X,
                        length: 1,
                        dir: Dir::Right,
                        c: TileContent::NtoW,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                TileContent::StoW => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::X,
                        length: 1,
                        dir: Dir::Right,
                        c: TileContent::StoW,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                TileContent::HorPipe => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::X,
                        length: 1,
                        dir: Dir::Right,
                        c: TileContent::HorPipe,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                _ => (),
            }
        }

        if let Some(x) = self.tiles.get(&(self.start + IVec2::Y)) {
            match x {
                TileContent::StoE => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::Y,
                        length: 1,
                        dir: Dir::Up,
                        c: TileContent::StoE,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                TileContent::StoW => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::Y,
                        length: 1,
                        dir: Dir::Up,
                        c: TileContent::StoW,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                TileContent::VertPipe => {
                    let new_meter = Meter {
                        pos: self.start + IVec2::Y,
                        length: 1,
                        dir: Dir::Up,
                        c: TileContent::VertPipe,
                    };
                    if self.path_1 == Meter::default() {
                        self.path_1 = new_meter;
                    } else if self.path_2 == Meter::default() {
                        self.path_2 = new_meter;
                    }
                }
                _ => (),
            }
        }
    }

    fn travel_path(&mut self, n: u8) -> u32 {
        let mut path = match n {
            1 => self.path_1,
            2 => self.path_2,
            _ => panic!(),
        };

        while true {
            let next_tile: TileContent = match path.dir {
                Dir::Up => *self.tiles.get(&(path.pos + IVec2::Y)).unwrap(),
                Dir::Down => *self.tiles.get(&(path.pos - IVec2::Y)).unwrap(),
                Dir::Left => *self.tiles.get(&(path.pos - IVec2::X)).unwrap(),
                Dir::Right => *self.tiles.get(&(path.pos + IVec2::X)).unwrap(),
            };
            if matches!(next_tile, TileContent::Start) {
                path.length += 1;
                break;
            }
            path = path.add(next_tile);
        }
        path.length
    }
}

fn parse_symbol(input: Span) -> IResult<Span, Span> {
    alt((
        tag::<_, _, Error<_>>("|"),
        tag("-"),
        tag("L"),
        tag("J"),
        tag("7"),
        tag("F"),
        tag("."),
        tag("S"),
        tag("\n"),
    ))(input)
}

fn match_symbol(input: Span) -> IResult<Span, (IVec2, TileContent)> {
    // let (span, res) = res.unwrap();

    let res: SpanIVec2 = with_xy(input);

    let res_2 = match *res.fragment() {
        "|" => TileContent::VertPipe,
        "-" => TileContent::HorPipe,
        "L" => TileContent::NtoE,
        "J" => TileContent::NtoW,
        "7" => TileContent::StoW,
        "F" => TileContent::StoE,
        "." => TileContent::Empty,
        "S" => TileContent::Start,
        "\n" => TileContent::NewLine,
        _ => panic!("unknown tile content"),
    };

    Ok((input, (res.extra, res_2)))
}

fn take1(s: Span) -> IResult<Span, Span> {
    let res = alt((
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("|"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("-"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("L"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("J"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("7"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("F"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("."),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("S"),
    ))(s);

    let res = res.unwrap();
    Ok(res)
}

fn parser(input: &str) -> Map {
    let input_span = Span::new(input);
    let line_count = input.lines().count();
    let mut tilemap: HashMap<IVec2, TileContent> = HashMap::new();

    let res: Vec<_> = iterator(input_span, parse_symbol).collect();
    res.iter()
        .map(|x| match_symbol(*x).unwrap().1)
        .for_each(|(pos, content)| {
            tilemap.insert(
                IVec2 {
                    x: pos.x,
                    y: line_count as i32 - pos.y - 1,
                },
                content,
            );
        });

    let mut tilemap: Map = Map {
        tiles: tilemap,
        ..Default::default()
    };
    tilemap
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        let result = part1(
            "..F7.
.FJ|.
SJ.L7
|F--J
LJ...",
        );
        assert_eq!(result, "8".to_string());
    }
    fn test_part_1_bis() {
        let result = part1(
            ".....
.S-7.
.|.|.
.L-J.
.....",
        );
        assert_eq!(result, "4".to_string());
    }
}
