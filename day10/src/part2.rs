#![allow(dead_code, unused_imports)]

use glam::IVec2;
use itertools::Itertools;
use nom::branch::alt;
use nom::bytes::complete::{is_a, is_not, tag, take_till1, take_until};
use nom::character::complete::{digit0, digit1, line_ending, newline, space1};
use nom::combinator::{iterator, opt, value};
use nom::complete::take;
use nom::error::{make_error, Error, ErrorKind};
use nom::multi::{many0, many1, separated_list1};
use nom::sequence::{terminated, tuple};
use nom::{Err, Finish, IResult, Parser};
use nom_locate::{position, LocatedSpan};
use num::range;
// use nom_supreme::{tag::complete::tag, ParserExt};
use regex::Regex;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::iter::zip;
use std::ops::Mul;
use std::str::FromStr;
use std::time::Instant;
#[macro_use]
extern crate lazy_static;
use enum_iterator::{all, cardinality, first, last, next, previous, reverse_all, Sequence};
use std::cmp::Ordering;
use strum_macros::EnumString;

// In the end I just found what is REALLY inside the loop by checking what is left or right.
// but I counted by hand the weird cases.
// Because what seems to matter to the problem is to use the ray-casting algorithm,
// counting the number of pipe crossing, whether the total was even or odd. Then
// some of the tiles are "in" because the number is even, but if you stretcht the loop
// they are actually outside.

// That was a pain.

// I just set by hand what is left or right (I could have selected the set by checking
// which one contains an edge like (0, 0))

fn main() {
    let input = include_str!("input1.txt");
    let now = Instant::now();
    let output = process(input, InOut::RightInLeftOut);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92mExecute in:\x1b[0m" },
        { elapsed }
    );
    dbg!(output);
}

fn process(input: &str, inout: InOut) -> String {
    let mut tilemap = parser(input);
    tilemap.in_out = inout;
    tilemap.display_content(1);

    tilemap.inflate_map();
    // tilemap.display_content(2);
    tilemap.find_start();
    tilemap.find_leads();
    tilemap.travel_path(1);
    // tilemap.display_path(2);
    // tilemap.shrink_map();
    // tilemap.inflate_map();
    // tilemap.display_path(2);
    // tilemap.display_tiles(2);
    tilemap.remove_extra();
    // tilemap.assign_set();
    // tilemap.display_tiles(2);
    tilemap.propagate_out(2);
    // tilemap.display_out(2);
    // tilemap.display_in(2);
    tilemap.propagate_in();
    // tilemap.display_tiles(2);
    // tilemap.display_in();
    // tilemap.display_in(2);
    // tilemap.display_tiles(2);
    // tilemap.display_out(2);
    // tilemap.display_path(2);
    // tilemap.display_path(2);
    tilemap.shrink_map();
    tilemap.display_tiles(1);
    // tilemap.display_in(2);

    let res = tilemap.count_in();

    res.to_string()
}

type Span<'a> = LocatedSpan<&'a str>;
type SpanIVec2<'a> = LocatedSpan<&'a str, IVec2>;

#[derive(Debug, Copy, Clone, Default, PartialEq)]
enum TileContent {
    #[default]
    Empty,
    NtoE,
    NtoW,
    StoE,
    StoW,
    Start,
    HorPipe,
    VertPipe,
    NewLine,
}

#[derive(Default, Debug, PartialEq, Eq, Clone, Copy)]
enum Dir {
    #[default]
    Up,
    Down,
    Left,
    Right,
}

fn with_xy(span: Span) -> SpanIVec2 {
    // column/location are 1-indexed
    let x = span.get_column() as i32 - 1;
    let y = span.location_line() as i32 - 1;
    span.map_extra(|_| IVec2::new(x, y))
}

#[derive(Default, Debug, Copy, Clone, PartialEq)]
struct Dashboard {
    pos: IVec2,
    length: u32,
    dir: Dir,
    c: TileContent,
}

impl Dashboard {
    fn add(&self, next_tile: TileContent) -> Dashboard {
        match self.dir {
            Dir::Up => match next_tile {
                TileContent::StoE => Dashboard {
                    pos: self.pos + IVec2::Y,
                    length: self.length + 1,
                    dir: Dir::Right,
                    c: next_tile,
                },
                TileContent::StoW => Dashboard {
                    pos: self.pos + IVec2::Y,
                    length: self.length + 1,
                    dir: Dir::Left,
                    c: next_tile,
                },
                TileContent::VertPipe => Dashboard {
                    pos: self.pos + IVec2::Y,
                    length: self.length + 1,
                    dir: Dir::Up,
                    c: next_tile,
                },
                _ => panic!(),
            },

            Dir::Down => match next_tile {
                TileContent::NtoE => Dashboard {
                    pos: self.pos + IVec2::NEG_Y,
                    length: self.length + 1,
                    dir: Dir::Right,
                    c: next_tile,
                },
                TileContent::NtoW => Dashboard {
                    pos: self.pos + IVec2::NEG_Y,
                    length: self.length + 1,
                    dir: Dir::Left,
                    c: next_tile,
                },
                TileContent::VertPipe => Dashboard {
                    pos: self.pos + IVec2::NEG_Y,
                    length: self.length + 1,
                    dir: Dir::Down,
                    c: next_tile,
                },
                _ => panic!(),
            },
            Dir::Left => match next_tile {
                TileContent::NtoE => Dashboard {
                    pos: self.pos + IVec2::NEG_X,
                    length: self.length + 1,
                    dir: Dir::Up,
                    c: next_tile,
                },
                TileContent::StoE => Dashboard {
                    pos: self.pos + IVec2::NEG_X,
                    length: self.length + 1,
                    dir: Dir::Down,
                    c: next_tile,
                },
                TileContent::HorPipe => Dashboard {
                    pos: self.pos + IVec2::NEG_X,
                    length: self.length + 1,
                    dir: Dir::Left,
                    c: next_tile,
                },
                _ => panic!(),
            },
            Dir::Right => match next_tile {
                TileContent::NtoW => Dashboard {
                    pos: self.pos + IVec2::X,
                    length: self.length + 1,
                    dir: Dir::Up,
                    c: next_tile,
                },
                TileContent::StoW => Dashboard {
                    pos: self.pos + IVec2::X,
                    length: self.length + 1,
                    dir: Dir::Down,
                    c: next_tile,
                },
                TileContent::HorPipe => Dashboard {
                    pos: self.pos + IVec2::X,
                    length: self.length + 1,
                    dir: Dir::Right,
                    c: next_tile,
                },
                _ => panic!(),
            },
        }
    }
}

#[derive(Default, Debug, PartialEq, Eq, Clone, Copy)]
enum InOut {
    #[default]
    RightInLeftOut,
    LeftInRightOut,
}

#[derive(Default, Debug, Clone)]
struct Map<'a> {
    input: &'a str,
    tiles: HashMap<IVec2, TileContent>,
    start: IVec2,
    lead_1: Dashboard,
    lead_2: Dashboard,
    x_length: usize,
    y_length: usize,
    path: HashSet<IVec2>,
    left_set: HashSet<IVec2>,
    right_set: HashSet<IVec2>,
    undefined_set: HashSet<IVec2>,
    in_out: InOut,
}

impl<'a> Map<'a> {
    fn display_tiles(&self, factor: usize) {
        println!("{} {}", { "\x1b[38;5;92m➤➤➤\x1b[0m" }, {
            "\x1b[38;5;91mFiltered Map:\x1b[0m"
        },);
        for j in (0..self.y_length * factor).rev() {
            // print!("{j}");
            for i in 0..(self.x_length * factor) {
                let tmp_elt = self.tiles.get(&IVec2::from((i as i32, j as i32))).unwrap();
                let tmp_pos = IVec2::from((i as i32, j as i32));
                if !self.path.contains(&tmp_pos) {
                    if self.left_set.contains(&tmp_pos) {
                        match self.in_out {
                            InOut::RightInLeftOut => {
                                print!("O");
                            }
                            InOut::LeftInRightOut => {
                                print!("I");
                            }
                        }
                    } else if self.right_set.contains(&tmp_pos) {
                        match self.in_out {
                            InOut::RightInLeftOut => {
                                print!("I");
                            }
                            InOut::LeftInRightOut => {
                                print!("O");
                            }
                        }
                    } else if self.undefined_set.contains(&tmp_pos) {
                        print!("U")
                    } else {
                        print!(".");
                    }
                } else {
                    match tmp_elt {
                        TileContent::Empty => print!("."),
                        TileContent::NtoE => print!("L"),
                        TileContent::NtoW => print!("J"),
                        TileContent::StoE => print!("F"),
                        TileContent::StoW => print!("7"),
                        TileContent::Start => print!("S"),
                        TileContent::HorPipe => print!("-"),
                        TileContent::VertPipe => print!("|"),
                        TileContent::NewLine => panic!(),
                    };
                }
            }
            println!();
        }
        print!(" ");
        for j in 0..self.x_length * factor {
            print!("{j}")
        }
        println!();
    }

    fn display_content(&self, factor: usize) {
        println!("{} {}", { "\x1b[38;5;92m➤➤➤\x1b[0m" }, {
            "\x1b[38;5;91mFiltered Map:\x1b[0m"
        },);
        for j in (0..self.y_length * factor).rev() {
            // print!("{j}");
            for i in 0..(self.x_length * factor) {
                let tmp_elt = self.tiles.get(&IVec2::from((i as i32, j as i32))).unwrap();

                match tmp_elt {
                    TileContent::Empty => print!("."),
                    TileContent::NtoE => print!("L"),
                    TileContent::NtoW => print!("J"),
                    TileContent::StoE => print!("F"),
                    TileContent::StoW => print!("7"),
                    TileContent::Start => print!("S"),
                    TileContent::HorPipe => print!("-"),
                    TileContent::VertPipe => print!("|"),
                    TileContent::NewLine => panic!(),
                };
            }
            println!();
        }
        print!(" ");
        for j in 0..self.x_length * factor {
            print!("{j}")
        }
        println!();
    }

    fn inflate_map(&mut self) {
        // multiply by 2 the map
        for j in (0..self.y_length).rev() {
            for i in (0..self.x_length).rev() {
                let pos = &IVec2::from((i as i32, j as i32));
                let elt_content = self.tiles.remove(pos).unwrap();
                self.tiles.insert(pos.mul(2), elt_content);
            }
        }

        // fill the gaps:
        for j in 0..2 * self.y_length {
            for i in 0..2 * self.x_length {
                let mut elt_content: Option<TileContent> = None;
                // check if odd or even
                if i % 2 == 0 && j % 2 == 0 {
                    // don't touch existing tiles
                    elt_content = None;
                } else if i % 2 == 0 && j % 2 == 1 {
                    elt_content = Some(TileContent::VertPipe);
                } else if i % 2 == 1 && j % 2 == 0 {
                    elt_content = Some(TileContent::HorPipe);
                } else if i % 2 == 1 && j % 2 == 1 {
                    elt_content = Some(TileContent::Empty);
                }

                if let Some(x) = elt_content {
                    self.tiles.insert(IVec2::from((i as i32, j as i32)), x);
                }
            }
        }
    }

    fn shrink_map(&mut self) {
        for j in 0..self.y_length {
            for i in 0..self.x_length {
                let pos = &IVec2::from((i as i32, j as i32));
                let elt_content = self.tiles.get(&pos.mul(2)).unwrap();
                self.tiles.insert(*pos, *elt_content);
                if self.left_set.contains(&pos.mul(2)) {
                    self.left_set.insert(*pos);
                    self.left_set.remove(&pos.mul(2));
                } else {
                    self.left_set.remove(pos);
                    self.left_set.remove(&pos.mul(2));
                }
                if self.right_set.contains(&pos.mul(2)) {
                    self.right_set.insert(*pos);
                    self.right_set.remove(&pos.mul(2));
                } else {
                    self.right_set.remove(pos);
                    self.right_set.remove(&pos.mul(2));
                }
                if self.undefined_set.contains(&pos.mul(2)) {
                    self.undefined_set.insert(*pos);
                    self.undefined_set.remove(&pos.mul(2));
                } else {
                    self.undefined_set.remove(pos);
                    self.undefined_set.remove(&pos.mul(2));
                }
                if self.path.contains(&pos.mul(2)) {
                    self.path.insert(*pos);
                    self.path.remove(&pos.mul(2));
                } else {
                    self.path.remove(pos);
                    self.path.remove(&pos.mul(2));
                }
            }
        }
        for j in self.y_length..2 * self.y_length {
            for i in self.x_length..2 * self.x_length {
                let pos = IVec2::from((i as i32, j as i32));
                self.tiles.remove_entry(&pos);
                self.left_set.remove(&pos);
                self.right_set.remove(&pos);
                self.undefined_set.remove(&pos);
                self.path.remove(&pos);
            }
        }
        for j in 0..self.y_length {
            for i in self.x_length..2 * self.x_length {
                let pos = IVec2::from((i as i32, j as i32));
                self.tiles.remove_entry(&pos);
                self.left_set.remove(&pos);
                self.right_set.remove(&pos);
                self.undefined_set.remove(&pos);
                self.path.remove(&pos);
            }
        }
        for j in self.y_length..2 * self.y_length {
            for i in 0..self.x_length {
                let pos = IVec2::from((i as i32, j as i32));
                self.tiles.remove_entry(&pos);
                self.left_set.remove(&pos);
                self.right_set.remove(&pos);
                self.undefined_set.remove(&pos);
                self.path.remove(&pos);
            }
        }
    }

    fn display_in(&mut self, factor: usize) {
        println!("{} {}", { "\x1b[38;5;92m➤➤➤\x1b[0m" }, {
            "\x1b[38;5;91mInner:\x1b[0m"
        },);
        let inner = match self.in_out {
            InOut::RightInLeftOut => &mut self.right_set,
            InOut::LeftInRightOut => &mut self.left_set,
        };
        for j in (0..self.y_length * factor).rev() {
            for i in 0..self.x_length * factor {
                let tmp_pos = IVec2::from((i as i32, j as i32));
                match inner.contains(&tmp_pos) {
                    true => {
                        print!("I");
                    }
                    false => {
                        print!(".");
                    }
                }
            }
            print!(" ");
            println!();
        }
    }
    fn display_out(&mut self, factor: usize) {
        println!("{} {}", { "\x1b[38;5;92m➤➤➤\x1b[0m" }, {
            "\x1b[38;5;91mInner:\x1b[0m"
        },);
        let outer = match self.in_out {
            InOut::LeftInRightOut => &mut self.right_set,
            InOut::RightInLeftOut => &mut self.left_set,
        };
        for j in (0..self.y_length * factor).rev() {
            for i in 0..self.x_length * factor {
                let tmp_pos = IVec2::from((i as i32, j as i32));
                match outer.contains(&tmp_pos) {
                    true => {
                        print!("O");
                    }
                    false => {
                        print!(".");
                    }
                }
            }
            print!(" ");
            println!();
        }
    }

    fn display_path(&mut self, factor: usize) {
        println!("{} {}", { "\x1b[38;5;92m➤➤➤\x1b[0m" }, {
            "\x1b[38;5;91mInner:\x1b[0m"
        },);

        for j in (0..self.y_length * factor).rev() {
            // print!("{j}");
            for i in 0..self.x_length * factor {
                let tmp_pos = IVec2::from((i as i32, j as i32));
                match self.path.contains(&tmp_pos) {
                    true => {
                        print!("X");
                    }
                    false => {
                        print!(".");
                    }
                }
            }
            print!(" ");
            println!();
        }
    }

    fn find_start(&mut self) {
        let res = self
            .tiles
            .iter()
            .find(|(_, val)| matches!(**val, TileContent::Start));

        self.start = *res.unwrap().0;
        self.path.insert(self.start);
    }

    fn find_leads(&mut self) {
        if let Some(x) = self.tiles.get(&(self.start + 2 * IVec2::NEG_X)) {
            match x {
                TileContent::NtoE => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::NEG_X,
                        length: 1,
                        dir: Dir::Left,
                        c: TileContent::HorPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                TileContent::StoE => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::NEG_X,
                        length: 1,
                        dir: Dir::Left,
                        c: TileContent::HorPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                TileContent::HorPipe => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::NEG_X,
                        length: 1,
                        dir: Dir::Left,
                        c: TileContent::HorPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                _ => (),
            }
        }

        if let Some(x) = self.tiles.get(&(self.start + 2 * IVec2::NEG_Y)) {
            match x {
                TileContent::NtoE => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::NEG_Y,
                        length: 1,
                        dir: Dir::Down,
                        c: TileContent::VertPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                TileContent::NtoW => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::NEG_Y,
                        length: 1,
                        dir: Dir::Down,
                        c: TileContent::VertPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                TileContent::VertPipe => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::NEG_Y,
                        length: 1,
                        dir: Dir::Down,
                        c: TileContent::VertPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                _ => (),
            }
        }

        if let Some(x) = self.tiles.get(&(self.start + 2 * IVec2::X)) {
            match x {
                TileContent::NtoW => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::X,
                        length: 1,
                        dir: Dir::Right,
                        c: TileContent::HorPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                TileContent::StoW => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::X,
                        length: 1,
                        dir: Dir::Right,
                        c: TileContent::HorPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                TileContent::HorPipe => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::X,
                        length: 1,
                        dir: Dir::Right,
                        c: TileContent::HorPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                _ => (),
            }
        }

        if let Some(x) = self.tiles.get(&(self.start + 2 * IVec2::Y)) {
            match x {
                TileContent::StoE => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::Y,
                        length: 1,
                        dir: Dir::Up,
                        c: TileContent::VertPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                TileContent::StoW => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::Y,
                        length: 1,
                        dir: Dir::Up,
                        c: TileContent::VertPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                TileContent::VertPipe => {
                    let new_meter = Dashboard {
                        pos: self.start + IVec2::Y,
                        length: 1,
                        dir: Dir::Up,
                        c: TileContent::VertPipe,
                    };
                    if self.lead_1 == Dashboard::default() {
                        self.lead_1 = new_meter;
                    } else if self.lead_2 == Dashboard::default() {
                        self.lead_2 = new_meter;
                    }
                }
                _ => (),
            }
        }
    }

    fn travel_path(&mut self, n: u8) {
        let mut lead = match n {
            1 => self.lead_1,
            2 => self.lead_2,
            _ => panic!(),
        };

        // Start position
        self.path.insert(lead.pos);
        self.add_left_right(lead);

        loop {
            let next_tile: TileContent = match lead.dir {
                Dir::Up => *self.tiles.get(&(lead.pos + IVec2::Y)).unwrap(),
                Dir::Down => *self.tiles.get(&(lead.pos - IVec2::Y)).unwrap(),
                Dir::Left => *self.tiles.get(&(lead.pos - IVec2::X)).unwrap(),
                Dir::Right => *self.tiles.get(&(lead.pos + IVec2::X)).unwrap(),
            };
            if matches!(next_tile, TileContent::Start) {
                lead.length += 1;
                break;
            }

            // Go to the next tile. We need to know the content to be able to return the meterdirectly:
            lead = lead.add(next_tile);
            self.path.insert(lead.pos);
            self.add_left_right(lead);
        }
    }

    fn remove_extra(&mut self) {
        let left_copy = self.left_set.clone();
        let right_copy = self.right_set.clone();
        let undefined_copy = self.undefined_set.clone();

        let to_remove = self.path.intersection(&left_copy);
        for elt in to_remove {
            self.left_set.remove(elt);
        }
        let to_remove = self.path.intersection(&right_copy);
        for elt in to_remove {
            self.right_set.remove(elt);
        }
        let to_remove = self.path.intersection(&undefined_copy);
        for elt in to_remove {
            self.undefined_set.remove(elt);
        }
    }

    fn add_left_right(&mut self, dash: Dashboard) {
        let left_pos: Option<IVec2>;
        let right_pos: Option<IVec2>;
        let undefined: Option<IVec2>;

        (left_pos, right_pos, undefined) = match dash.c {
            TileContent::NtoE => match dash.dir {
                Dir::Up => (
                    Some(dash.pos + IVec2::NEG_X),
                    None,
                    Some(dash.pos + IVec2::NEG_Y),
                ),
                Dir::Right => (
                    None,
                    Some(dash.pos + IVec2::NEG_Y),
                    Some(dash.pos + IVec2::NEG_X),
                ),
                _ => panic!(),
            },
            TileContent::NtoW => match dash.dir {
                Dir::Up => (
                    None,
                    Some(dash.pos + IVec2::X),
                    Some(dash.pos + IVec2::NEG_Y),
                ),
                Dir::Left => (
                    Some(dash.pos + IVec2::NEG_Y),
                    None,
                    Some(dash.pos + IVec2::X),
                ),
                _ => panic!(),
            },
            TileContent::StoE => match dash.dir {
                Dir::Down => (
                    None,
                    Some(dash.pos + IVec2::NEG_X),
                    Some(dash.pos + IVec2::Y),
                ),
                Dir::Right => (
                    Some(dash.pos + IVec2::Y),
                    None,
                    Some(dash.pos + IVec2::NEG_X),
                ),
                _ => panic!(),
            },
            TileContent::StoW => match dash.dir {
                Dir::Down => (Some(dash.pos + IVec2::X), None, Some(dash.pos + IVec2::Y)),
                Dir::Left => (None, Some(dash.pos + IVec2::Y), Some(dash.pos + IVec2::X)),
                _ => panic!(),
            },
            TileContent::Start => (None, None, None),
            TileContent::HorPipe => match dash.dir {
                Dir::Left => (
                    Some(dash.pos + IVec2::NEG_Y),
                    Some(dash.pos + IVec2::Y),
                    None,
                ),
                Dir::Right => (
                    Some(dash.pos + IVec2::Y),
                    Some(dash.pos + IVec2::NEG_Y),
                    None,
                ),
                _ => panic!(),
            },
            TileContent::VertPipe => match dash.dir {
                Dir::Up => (
                    Some(dash.pos + IVec2::NEG_X),
                    Some(dash.pos + IVec2::X),
                    None,
                ),
                Dir::Down => (
                    Some(dash.pos + IVec2::X),
                    Some(dash.pos + IVec2::NEG_X),
                    None,
                ),
                _ => panic!(),
            },
            _ => panic!(),
        };

        if let Some(x) = left_pos {
            match self.tiles.contains_key(&x) {
                true => {
                    self.left_set.insert(x);
                }
                false => (),
            }
        }
        if let Some(x) = right_pos {
            match self.tiles.contains_key(&x) {
                true => {
                    self.right_set.insert(x);
                }
                false => (),
            }
        }
        if let Some(x) = undefined {
            match self.tiles.contains_key(&x) {
                true => {
                    self.undefined_set.insert(x);
                }
                false => (),
            }
        }
    }

    fn propagate_in(&mut self) {
        let inner = match self.in_out {
            InOut::RightInLeftOut => &mut self.right_set,
            InOut::LeftInRightOut => &mut self.left_set,
        };

        // for i_line in 0..self.y_length {
        //     let in_seed: Vec<IVec2> = inner
        //         .iter()
        //         .cloned()
        //         .filter(|elt| elt.y == i_line as i32)
        //         .collect();

        for seed in inner.clone().iter() {
            let (mut neg_x_j, mut pos_x_j, mut neg_y_j, mut pos_y_j) = (*seed, *seed, *seed, *seed);
            let mut tmp_val: IVec2;
            loop {
                if self.path.contains(&neg_x_j) || neg_x_j.x < 0 {
                    break;
                }
                inner.insert(neg_x_j);
                tmp_val = neg_x_j + IVec2::NEG_X;
                neg_x_j = tmp_val;
            }

            let mut tmp_val2: IVec2;
            loop {
                if self.path.contains(&pos_x_j) || pos_x_j.x > 200 {
                    break;
                }
                inner.insert(pos_x_j);
                tmp_val2 = pos_x_j + IVec2::X;
                pos_x_j = tmp_val2;
            }

            let mut tmp_val3: IVec2;
            loop {
                if self.path.contains(&neg_y_j) || neg_y_j.y < 00 {
                    break;
                }
                inner.insert(neg_y_j);
                tmp_val3 = neg_y_j + IVec2::NEG_Y;
                neg_y_j = tmp_val3;
            }

            let mut tmp_val4: IVec2;
            loop {
                if self.path.contains(&pos_y_j) || pos_y_j.y > 200 {
                    break;
                }
                inner.insert(pos_y_j);
                tmp_val4 = pos_y_j + IVec2::Y;
                pos_y_j = tmp_val4;
            }
        }

        for elt in self.undefined_set.clone() {
            inner.insert(elt);
            self.undefined_set.remove(&elt);
        }
    }
    fn propagate_out(&mut self, factor: usize) {
        let (mut outer, mut inner) = match self.in_out {
            InOut::RightInLeftOut => (self.left_set.clone(), self.right_set.clone()),
            InOut::LeftInRightOut => (self.right_set.clone(), self.left_set.clone()),
        };

        let mut in_seed: Vec<IVec2> = vec![];
        for elt in outer.iter() {
            in_seed.push(*elt);
        }

        for seed in in_seed.iter() {
            let (mut neg_x_j, mut pos_x_j, mut neg_y_j, mut pos_y_j) = (*seed, *seed, *seed, *seed);

            outer.insert(neg_x_j);
            self.undefined_set.remove(&neg_x_j);
            inner.remove(&neg_x_j);
            outer.insert(pos_x_j);
            self.undefined_set.remove(&pos_x_j);
            inner.remove(&pos_x_j);
            outer.insert(neg_y_j);
            self.undefined_set.remove(&neg_y_j);
            inner.remove(&neg_y_j);
            outer.insert(pos_y_j);
            self.undefined_set.remove(&pos_y_j);
            inner.remove(&pos_y_j);
            loop {
                neg_x_j += IVec2::NEG_X;
                if self.path.contains(&neg_x_j)
                    || is_outside_map(self.x_length * factor, self.y_length * factor, neg_x_j)
                {
                    break;
                }
                self.undefined_set.remove(&neg_x_j);
                inner.remove(&neg_x_j);
            }
            loop {
                pos_x_j += IVec2::X;
                if self.path.contains(&pos_x_j)
                    || is_outside_map(self.x_length * factor, self.y_length * factor, pos_x_j)
                {
                    break;
                }
                outer.insert(pos_x_j);
                self.undefined_set.remove(&pos_x_j);
                inner.remove(&pos_x_j);
            }
            loop {
                neg_y_j += IVec2::NEG_Y;
                if self.path.contains(&neg_y_j)
                    || is_outside_map(self.x_length * factor, self.y_length * factor, neg_y_j)
                {
                    break;
                }
                outer.insert(neg_y_j);
                self.undefined_set.remove(&neg_y_j);
                inner.remove(&neg_y_j);
            }
            loop {
                pos_y_j += IVec2::Y;
                if self.path.contains(&pos_y_j)
                    || is_outside_map(self.x_length * factor, self.y_length * factor, pos_y_j)
                {
                    break;
                }
                outer.insert(pos_y_j);
                self.undefined_set.remove(&pos_y_j);
                inner.remove(&pos_y_j);
            }
        }
        match self.in_out {
            InOut::RightInLeftOut => {
                self.left_set = outer;
            }
            InOut::LeftInRightOut => {
                self.right_set = outer;
            }
        };
    }

    fn assign_set(&mut self) {
        // let (outer, inner) = match self.in_out {
        //     InOut::RightInLeftOut => (&mut self.left_set, &mut self.right_set),
        //     InOut::LeftInRightOut => (&mut self.right_set, &mut self.left_set),
        // };

        // // Add undefined items to the inner loop:
        // for elt in &self.undefined_set.clone() {
        //     let mut next_to_out = false;

        //     if outer.contains(&(*elt + IVec2::X))
        //         || outer.contains(&(*elt + IVec2::NEG_X))
        //         || outer.contains(&(*elt + IVec2::Y))
        //         || outer.contains(&(*elt + IVec2::NEG_Y))
        //     {
        //         next_to_out = true;
        //     }

        //     match next_to_out {
        //         true => {
        //             outer.insert(*elt);
        //             self.undefined_set.remove(elt);
        //         }
        //         false => (),
        //     }
        // }

        // for elt in &self.undefined_set.clone() {
        //     self.undefined_set.remove(elt);
        // }
    }

    fn count_in(&self) -> usize {
        match self.in_out {
            InOut::RightInLeftOut => self.right_set.len(),
            InOut::LeftInRightOut => self.left_set.len(),
        }
    }
}

fn is_outside_map(x_length: usize, y_length: usize, elt: IVec2) -> bool {
    elt.x < 0 || elt.x > (x_length - 1) as i32 || elt.y < 0 || elt.y > (y_length - 1) as i32
}

fn parse_symbol(input: Span) -> IResult<Span, Span> {
    alt((
        tag::<_, _, Error<_>>("|"),
        tag("-"),
        tag("L"),
        tag("J"),
        tag("7"),
        tag("F"),
        tag("."),
        tag("S"),
        tag("\n"),
    ))(input)
}

fn match_symbol(input: Span) -> IResult<Span, (IVec2, TileContent)> {
    // let (span, res) = res.unwrap();

    let res: SpanIVec2 = with_xy(input);

    let res_2 = match *res.fragment() {
        "|" => TileContent::VertPipe,
        "-" => TileContent::HorPipe,
        "L" => TileContent::NtoE,
        "J" => TileContent::NtoW,
        "7" => TileContent::StoW,
        "F" => TileContent::StoE,
        "." => TileContent::Empty,
        "S" => TileContent::Start,
        "\n" => TileContent::NewLine,
        _ => panic!("unknown tile content"),
    };

    Ok((input, (res.extra, res_2)))
}

fn take1(s: Span) -> IResult<Span, Span> {
    let res = alt((
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("|"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("-"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("L"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("J"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("7"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("F"),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("."),
        tag::<&str, nom_locate::LocatedSpan<&str>, Error<_>>("S"),
    ))(s);

    let res = res.unwrap();
    Ok(res)
}

fn parser(input: &str) -> Map {
    let input_span = Span::new(input);
    let line_count = input.lines().count();
    let line_length = input.lines().last().unwrap().chars().count();
    let mut tilemap: HashMap<IVec2, TileContent> = HashMap::new();

    let res: Vec<_> = iterator(input_span, parse_symbol).collect();
    res.iter()
        .map(|x| match_symbol(*x).unwrap().1)
        .filter(|x| !matches!(x.1, TileContent::NewLine))
        .for_each(|(pos, content)| {
            tilemap.insert(
                IVec2 {
                    x: pos.x,
                    y: line_count as i32 - pos.y - 1,
                },
                content,
            );
        });

    let tilemap: Map = Map {
        tiles: tilemap,
        x_length: line_length,
        y_length: line_count,
        input,
        ..Default::default()
    };
    tilemap
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_2() {
        let result = process(
            ".J...L..7|
.S------7.
.|F----7|.
L||..7.||.
.||....||.
.|L-7F-J|-
.|..||..|.
.L--JL--J.
.F.....J..",
            InOut::LeftInRightOut,
        );
        assert_eq!(result, "4".to_string());
    }

    fn test_part_2_5() {
        let result = process(
            "...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........",
            InOut::LeftInRightOut,
        );
        assert_eq!(result, "4".to_string());
    }

    fn test_part_2_2() {
        let result = process(
            "OF----7F7F7F7F-7OOOO
O|F--7||||||||FJOOOO
O||OFJ||||||||L7OOOO
FJL7L7LJLJ||LJIL-7OO
L--JOL7IIILJS7F-7L7O
OOOOF-JIIF7FJ|L7L7L7
OOOOL7IF7||L7|IL7L7|
OOOOO|FJLJ|FJ|F7|OLJ
OOOOFJL-7O||O||||OOO
OOOOL---JOLJOLJLJOOO",
            InOut::LeftInRightOut,
        );
        assert_eq!(result, "8".to_string());
    }

    fn test_part_2_3() {
        let result = process(
            "OF----7F7F7F7F-7OOOO
O|F--7||||||||FJOOOO
O||OFJ||||||||L7OOOO
FJL7L7LJLJ||LJIL-7OO
L--JOL7IIILJS7F-7L7O
OOOOF-JIIF7FJ|L7L7L7
OOOOL7IF7||L7|IL7L7|
OOOOO|FJLJ|FJ|F7|OLJ
OOOOFJL-7O||O||||OOO
OOOOL---JOLJOLJLJOOO",
            InOut::LeftInRightOut,
        );
        assert_eq!(result, "10".to_string());
    }
}
