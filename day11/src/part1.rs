#![allow(dead_code, unused_imports)]

use glam::IVec2;
use itertools::Itertools;
use nom::{
    branch::alt, bytes::complete::tag, character::complete::multispace0, combinator::all_consuming,
    multi::many1, sequence::terminated, IResult, Parser,
};
use nom_locate::LocatedSpan;
// use nom_supreme::{tag::complete::tag, ParserExt};
use regex::Regex;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::iter::zip;
use std::str::FromStr;
use std::time::Instant;
#[macro_use]
extern crate lazy_static;
use enum_iterator::{all, cardinality, first, last, next, previous, reverse_all, Sequence};
use std::cmp::Ordering;
use strum_macros::EnumString;

fn main() {
    let input = include_str!("input1.txt");
    let now = Instant::now();
    let output = part1(input);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92mExecute in:\x1b[0m" },
        { elapsed }
    );
    dbg!(output);
}

fn part1(input: &str) -> String {
    let mut tilemap = parser(input);

    tilemap.find_empty_space();
    tilemap.display_tiles();
    tilemap.inflate_map();
    tilemap.display_tiles();
    let res = tilemap.find_paths();

    let res: usize = res.iter().sum();

    res.to_string()
}

type Span<'a> = LocatedSpan<&'a str>;
type SpanIVec2<'a> = LocatedSpan<&'a str, IVec2>;

fn with_xy(span: Span) -> SpanIVec2 {
    // column/location are 1-indexed
    let x = span.get_column() as i32 - 1;
    let y = span.location_line() as i32 - 1;
    span.map_extra(|_| IVec2::new(x, y))
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum TileContent {
    Galaxy,
    Empty,
}

#[derive(Default, Debug, Clone)]
struct Space {
    galaxies: HashSet<IVec2>,
    empty_rows: Vec<usize>,
    empty_col: Vec<usize>,
    x_length: usize,
    y_length: usize,
    paths: HashMap<HashSet<IVec2>, usize>,
}

#[derive(Debug)]
struct SpaceInfo<'a> {
    span: SpanIVec2<'a>,
    space_type: TileContent,
}

impl Space {
    fn inflate_map(&mut self) {
        // Insert empty cols:
        for i_empty in self.empty_col.iter().rev() {
            self.x_length += 1;
            let tile_cp: HashSet<IVec2> = self
                .galaxies
                .iter()
                .map(|pos: &IVec2| {
                    // let is_gal = tiles_cp.contains(pos);S
                    if pos.x > *i_empty as i32 {
                        *pos + IVec2::X
                    } else {
                        *pos
                    }
                })
                .collect();
            self.galaxies = tile_cp;
        }
        // insert empty rows:
        for j_empty in self.empty_rows.iter().rev() {
            self.y_length += 1;
            let tile_cp: HashSet<IVec2> = self
                .galaxies
                .iter()
                .map(|pos: &IVec2| {
                    // let is_gal = tiles_cp.contains(pos);S
                    if pos.y > *j_empty as i32 {
                        *pos + IVec2::Y
                    } else {
                        *pos
                    }
                })
                .collect();
            self.galaxies = tile_cp;
        }
    }

    fn find_empty_space(&mut self) {
        for j in 0..self.y_length {
            let mut is_empty = true;
            for i in 0..self.x_length {
                let pos = &IVec2::from((i as i32, j as i32));
                is_empty = is_empty && !self.galaxies.contains(pos);
            }
            if is_empty {
                self.empty_rows.push(j);
            }
        }
        for i in 0..self.x_length {
            let mut is_empty = true;
            for j in 0..self.y_length {
                let pos = &IVec2::from((i as i32, j as i32));
                is_empty = is_empty && !self.galaxies.contains(pos)
            }
            if is_empty {
                self.empty_col.push(i);
            }
        }
    }

    fn find_paths(&self) -> Vec<usize> {
        // use tuple_combinations from itertools
        let paths: Vec<usize> = self
            .galaxies
            .iter()
            .tuple_combinations::<(_, _)>()
            .map(|(a, b)| ((a.x - b.x).abs() + (a.y - b.y).abs()) as usize)
            .collect();
        paths
    }

    fn display_tiles(&self) {
        for j in 0..self.y_length {
            for i in 0..(self.x_length) {
                if self.galaxies.contains(&IVec2::from((i as i32, j as i32))) {
                    print!("X")
                } else {
                    print!(".")
                }
            }
            println!();
        }
        println!();
    }
}

fn parser(input: &str) -> Space {
    let (_input, grid) = parse_grid(Span::new(input)).expect("should parse a valid grid");

    let line_count = input.lines().count();
    let line_length = input.lines().last().unwrap().chars().count();

    Space {
        galaxies: grid,
        x_length: line_length,
        y_length: line_count,
        ..Default::default()
    }
}

fn parse_grid(input: Span) -> IResult<Span, HashSet<IVec2>> {
    let (input, output) = all_consuming(many1(terminated(
        alt((
            tag("#").map(with_xy).map(|span| SpaceInfo {
                span,
                space_type: TileContent::Galaxy,
            }),
            tag(".").map(with_xy).map(|span| SpaceInfo {
                span,
                space_type: TileContent::Empty,
            }),
        )),
        multispace0,
    )))(input)?;

    Ok((
        input,
        output
            .into_iter()
            .filter_map(|space_info| {
                (space_info.space_type == TileContent::Galaxy).then_some(space_info.span.extra)
            })
            .collect(),
    ))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        let result = part1(
            "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....",
        );
        assert_eq!(result, "374".to_string());
    }
}
