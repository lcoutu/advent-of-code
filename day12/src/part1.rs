#![allow(dead_code, unused_imports)]

use glam::IVec2;
use itertools::{interleave, Combinations, Itertools};
use nom::character::complete::{digit1, line_ending, newline, space1};
use nom::combinator::{eof, recognize};
use nom::multi::separated_list1;
use nom::sequence::separated_pair;
use nom::{
    branch::alt, bytes::complete::tag, character::complete::multispace0, combinator::all_consuming,
    multi::many1, sequence::terminated, IResult, Parser,
};
use nom_locate::LocatedSpan;
use num::integer::binomial;
use rayon::iter::{IntoParallelIterator, IntoParallelRefIterator, ParallelIterator};
// use nom_supreme::{tag::complete::tag, ParserExt};
use regex::Regex;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::iter::zip;
use std::str::FromStr;
use std::time::Instant;
#[macro_use]
extern crate lazy_static;
use enum_iterator::{all, cardinality, first, last, next, previous, reverse_all, Sequence};
use itertools::FoldWhile::{Continue, Done};
use std::cmp::Ordering;
use strum_macros::EnumString;

fn main() {
    let input = include_str!("input1.txt");
    let now = Instant::now();
    let output = part1(input);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92mExecute in:\x1b[0m" },
        { elapsed }
    );
    dbg!(output);
}

fn part1(input: &str) -> String {
    // println!(
    //     "{} {} {:?}",
    //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
    //     { "\x1b[38;5;92m AAA:\x1b[0m" },
    //     { input }
    // );
    let entries = parser(input);

    let res: usize = entries.par_iter().map(|x| x.compute_arrs()).sum();

    res.to_string()
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Status {
    Operational,
    Damaged,
    Unknown,
}

#[derive(Default, Debug, Clone)]
struct Entry {
    statuses: BTreeMap<usize, Status>,
    arrs: Vec<usize>,
}

impl Entry {
    fn compute_arrs(&self) -> usize {
        // println!(
        //     "{} {} {:?}",
        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        //     { "\x1b[38;5;92m AAA:\x1b[0m" },
        //     { self.statuses.clone() }
        // );
        // println!(
        //     "{} {} {:?}",
        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        //     { "\x1b[38;5;92m BBB:\x1b[0m" },
        //     { self.arrs.clone() }
        // );

        let combis = self.all_combis();

        let res = combis
            .iter()
            .map(|x| self.match_combi(x))
            .filter(|x| *x)
            .count();

        let tmp_val = combis
            .iter()
            .map(|x| (x, self.match_combi(x)))
            .collect::<Vec<(&Vec<usize>, bool)>>();
        // println!(
        //     "{} {} {:?}",
        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        //     { "\x1b[38;5;92m CCC:\x1b[0m" },
        //     { tmp_val }
        // );
        res
    }

    fn all_combis(&self) -> Vec<Vec<usize>> {
        let damaged_nb = self.damage_nb();

        let target_nb: usize = self.arrs.iter().sum();
        let combi_nb = target_nb - damaged_nb;
        let unkown_pos = self.unkown_pos();
        let all_combis: Vec<Vec<usize>> = unkown_pos.into_iter().combinations(combi_nb).collect();
        all_combis
    }

    fn unkown_pos(&self) -> Vec<usize> {
        self.statuses
            .clone()
            .iter()
            .filter_map(|(i, x)| match x {
                Status::Operational | Status::Damaged => None,

                Status::Unknown => Some(*i),
            })
            .collect()
    }

    fn damage_nb(&self) -> usize {
        self.statuses
            .iter()
            .filter(|(_, x)| matches!(**x, Status::Damaged))
            .count()
    }
    // (a, b) in depths.iter().zip(depths.iter().skip(1))
    fn match_combi(&self, ukn_combi: &[usize]) -> bool {
        let mut i_group: usize = 0;
        let mut group_len = self.arrs[i_group];
        let mut counter: usize = 0;
        // println!(
        //     "{} {} {:?}",
        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        //     { "\x1b[38;5;92m EEE:\x1b[0m" },
        //     { ukn_combi }
        // );
        for (key, val) in self.statuses.iter() {
            // replace unknow tiles
            let tmp_val: Status;
            if matches!(val, Status::Unknown) {
                if ukn_combi.contains(key) {
                    tmp_val = Status::Damaged;
                } else {
                    tmp_val = Status::Operational;
                }
            } else {
                tmp_val = *val;
            }

            // println!(
            //     "{} {} {:?}",
            //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
            //     { "\x1b[38;5;92m DDD:\x1b[0m" },
            //     { (i_group, group_len, counter, tmp_val) }
            // );
            // check if it matches the expected lengths
            match tmp_val {
                Status::Operational => {
                    if counter > 0 && counter != group_len {
                        return false;
                    } else if counter > 0 && counter == group_len {
                        if i_group == self.arrs.len() - 1 {
                            return true;
                        }
                        i_group += 1;
                        group_len = self.arrs[i_group];
                        counter = 0;
                    } else if counter == 0 {
                    } else {
                        panic!("unexpected case")
                    }
                }
                Status::Damaged => {
                    counter += 1;
                }
                Status::Unknown => panic!(),
            }
        }

        counter == group_len
    }
}

fn parser(input: &str) -> Vec<Entry> {
    let (_, entries) = parse_str(input).unwrap();

    entries
}

fn parse_status(input: &str) -> IResult<&str, Vec<Status>> {
    let (remain, res) = many1(alt((tag("."), tag("#"), tag("?"))))(input)?;

    let res: Vec<Status> = res
        .iter()
        .map(|x| match *x {
            "#" => Status::Damaged,
            "." => Status::Operational,
            "?" => Status::Unknown,
            _ => panic!(),
        })
        .collect();

    Ok((remain, res))
}

fn parse_arr(input: &str) -> IResult<&str, Vec<usize>> {
    let (remain, res) = separated_list1(tag(","), digit1)(input)?;

    Ok((
        remain,
        res.iter().map(|x| x.parse::<usize>().unwrap()).collect(),
    ))
}

fn parse_str(input: &str) -> IResult<&str, Vec<Entry>> {
    let (input, output) = many1(terminated(
        separated_pair(parse_status, space1, parse_arr),
        alt((recognize(eof), recognize(newline))),
    ))(input)?;

    let res = output
        .into_iter()
        .map(|(s, a)| Entry {
            statuses: s.into_iter().enumerate().collect(),
            arrs: a,
        })
        .collect();
    Ok((input, res))
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[rstest]
    #[case("?.??..??#.? 2,2", "1")]
    #[case("???.### 1,1,3", "1")]
    #[case(".??..??...?##. 1,1,3", "4")]
    #[case("?#?#?#?#?#?#?#? 1,3,1,6", "1")]
    #[case("????.#...#... 4,1,1", "1")]
    #[case("????.######..#####. 1,6,5", "4")]
    #[case("?###???????? 3,2,1", "10")]
    #[case(
        "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1",
        "21"
    )]
    fn test_part_1(#[case] input: &str, #[case] output: &str) {
        let result = part1(input);
        assert_eq!(result, output.to_string());
    }
}
