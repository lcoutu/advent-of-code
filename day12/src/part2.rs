#![allow(dead_code, unused_imports)]

use glam::IVec2;
use itertools::{interleave, repeat_n, Combinations, Itertools};
use nom::character::complete::{digit1, line_ending, newline, space1};
use nom::combinator::{eof, recognize};
use nom::multi::separated_list1;
use nom::sequence::separated_pair;
use nom::{
    branch::alt, bytes::complete::tag, character::complete::multispace0, combinator::all_consuming,
    multi::many1, sequence::terminated, IResult, Parser,
};
use nom_locate::LocatedSpan;
use num::integer::binomial;
use rayon::iter::{IntoParallelIterator, IntoParallelRefIterator, ParallelIterator};
// use nom_supreme::{tag::complete::tag, ParserExt};
use regex::Regex;
use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::default;
use std::iter::zip;
use std::rc::Rc;
use std::str::FromStr;
use std::thread::{self, current};
use std::time::Instant;
#[macro_use]
extern crate lazy_static;
use enum_iterator::{all, cardinality, first, last, next, previous, reverse_all, Sequence};
use itertools::FoldWhile::{Continue, Done};
use std::cmp::Ordering;
use strum_macros::EnumString;

fn main() {
    let input = include_str!("input1.txt");
    let now = Instant::now();
    let output = part2(input);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92mExecute in:\x1b[0m" },
        { elapsed }
    );
    dbg!(output);
}

fn part2(input: &str) -> String {
    // println!(
    //     "{} {} {:?}",
    //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
    //     { "\x1b[38;5;92m AAA:\x1b[0m" },
    //     { input }
    // );
    let entries = parser(input);

    // println!(
    //     "{} {} {:?}",
    //     { "\x1b[38;5;92m⮞⮞⮞⮞⮞⮞⮞⮞⮞\x1b[0m" },
    //     { "\x1b[38;5;92m KKK:\x1b[0m" },
    //     { entries.clone() }
    // );
    // entries = expand_entries(entries);

    let res: usize = entries.par_iter().map(|x| x.clone().compute_arrs()).sum();

    res.to_string()
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default, Hash)]
enum Status {
    #[default]
    Operational,
    Damaged,
    Unknown,
}

#[derive(Default, Debug, Clone)]
struct Entry {
    statuses: Vec<Status>,
    arrs: Vec<usize>,
    damaged_nb: usize,
    target_nb: usize,
    unkown_nb: usize,
}

fn expand_entries(input: Vec<Entry>) -> Vec<Entry> {
    // println!(
    //     "{} {} {:?}",
    //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
    //     { "\x1b[38;5;92m AAA:\x1b[0m" },
    //     {}
    // );
    // for elt in input.iter() {
    //     elt.display()
    // }

    let mut res: Vec<Entry> = input
        .iter()
        .map(|x| {
            let mut statuses: Vec<Status> = vec![];
            let mut arrs: Vec<usize> = vec![];
            let mut i: usize = 0;
            for _ in 0..5 {
                for value in x.statuses.iter() {
                    statuses.insert(i, *value);
                    i += 1;
                }
                statuses.insert(i, Status::Unknown);
                i += 1;
                arrs.append(&mut x.arrs.clone());
            }
            Entry {
                statuses,
                arrs,
                ..Default::default()
            }
        })
        .collect();

    res.iter_mut().for_each(|x| {
        x.statuses.pop();
    });

    // for elt in res.iter() {
    //     elt.display()
    // }
    res
}

impl Entry {
    fn display(&self) {
        for elt in self.statuses.clone() {
            match elt {
                Status::Operational => print!("."),
                Status::Damaged => print!("#"),
                Status::Unknown => print!("?"),
            }
        }

        print!("{:?}", { self.arrs.clone() });
        println!();
    }

    fn add(&self, other: Entry) -> Entry {
        Entry {
            statuses: [self.statuses.clone(), other.statuses].concat(),
            arrs: [self.arrs.clone(), other.arrs].concat(),
            damaged_nb: self.damaged_nb + other.damaged_nb,
            target_nb: self.target_nb + other.target_nb,
            unkown_nb: self.unkown_nb + other.unkown_nb,
        }
    }

    fn compute_arrs(&mut self) -> usize {
        self.damaged_nb = self.arrs.iter().sum();
        self.unkown_nb = self.unkown_nb();

        let cache: Rc<
            RefCell<HashMap<(Status, Status, usize, &[Status], &[usize], usize), usize>>,
        > = Rc::new(RefCell::new(HashMap::new()));

        let mut cur_entry = self.clone();

        let mut res: usize = 0;
        let hold_entry = cur_entry.clone();
        res = self.cached_recur_combis(
            cache.clone(),
            Status::Operational,
            Status::Operational,
            self.clone().arrs[0],
            &hold_entry.statuses,
            &hold_entry.arrs[1..],
            self.damaged_nb,
            "".to_string(),
        );

        let splitter = Entry {
            statuses: vec![Status::Unknown],
            ..Default::default()
        };

        let mut entries: Vec<Entry> = vec![self.clone()];
        for _ in 0..4 {
            cur_entry = cur_entry.clone().add(splitter.clone()).add(self.clone());
            // cur_entry.display();
            entries.push(cur_entry.clone());
        }
        // println!(
        //     "{} {} {:?}",
        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        //     { "\x1b[38;5;92m AAA:\x1b[0m" },
        //     { entries.clone() }
        // );

        for (i, entry) in entries.iter().enumerate() {
            res = self.cached_recur_combis(
                cache.clone(),
                Status::Operational,
                Status::Operational,
                entry.clone().arrs[0],
                &entry.statuses,
                &entry.arrs[1..],
                self.damaged_nb * (i + 1),
                "".to_string(),
            );
        }

        res
    }

    fn cached_recur_combis<'a>(
        &self,
        cache: Rc<
            RefCell<HashMap<(Status, Status, usize, &'a [Status], &'a [usize], usize), usize>>,
        >,
        prev_elt: Status,
        cur_elt: Status,
        cur_size: usize,
        input: &'a [Status],
        sizes: &'a [usize],
        remain_damaged: usize,
        history: String,
    ) -> usize {
        let res = self.recursive_combis(
            cache.clone(),
            prev_elt,
            cur_elt,
            cur_size,
            input,
            sizes,
            remain_damaged,
            history,
        );

        let mut actual_cache = cache.borrow_mut();

        actual_cache.insert(
            (prev_elt, cur_elt, cur_size, input, sizes, remain_damaged),
            res,
        );

        // println!(
        //     "{} {} {:?}",
        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        //     { "\x1b[38;5;92m res:\x1b[0m" },
        //     { res }
        // );
        // print!(" ");
        // for elt in input {
        //     match elt {
        //         Status::Operational => print!("."),
        //         Status::Damaged => print!("#"),
        //         Status::Unknown => print!("?"),
        //     }
        // }
        // println!();
        res
    }

    fn recursive_combis<'a>(
        &self,
        cache: Rc<
            RefCell<HashMap<(Status, Status, usize, &'a [Status], &'a [usize], usize), usize>>,
        >,
        prev_elt: Status,
        cur_elt: Status,
        cur_size: usize,
        input: &'a [Status],
        sizes: &'a [usize],
        remain_damaged: usize,
        history: String,
    ) -> usize {
        let cur_elt_str = match cur_elt {
            Status::Operational => ".",
            Status::Damaged => "#",
            Status::Unknown => "?",
        };

        // print!("{}", history.clone());
        // print!("[");
        // match cur_elt {
        //     Status::Operational => print!("."),
        //     Status::Damaged => print!("#"),
        //     Status::Unknown => print!("?"),
        // }
        // print!("]");
        // for elt in input {
        //     match elt {
        //         Status::Operational => print!("."),
        //         Status::Damaged => print!("#"),
        //         Status::Unknown => print!("?"),
        //     }
        // }
        // print!(
        //     "          {} {} {:?}",
        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        //     { "\x1b[38;5;92m BBB:\x1b[0m" },
        //     { (prev_elt, cur_elt, cur_size, sizes, remain_damaged) }
        // );
        // println!();

        // reched the end of the line
        if input.is_empty() {
            // we place all the damaged springs:
            match cur_elt {
                Status::Operational => {
                    if remain_damaged == 0 {
                        // println!(
                        //     "{} {} {:?}",
                        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                        //     { "\x1b[38;5;92m LLL:\x1b[0m" },
                        //     { history.clone() }
                        // );
                        return 1;
                    } else {
                        // println!(
                        //     "{} {} {:?}",
                        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                        //     { "\x1b[38;5;92m XXX:\x1b[0m" },
                        //     { history.clone() }
                        // );
                        return 0;
                    }
                }
                Status::Damaged => {
                    if remain_damaged == 1 {
                        if cur_size == 1 {
                            // println!(
                            //     "{} {} {}{}",
                            //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                            //     { "\x1b[38;5;92m LLL:\x1b[0m" },
                            //     { history.clone() },
                            //     { "#" }
                            // );
                            return 1;
                        } else {
                            // println!(
                            //     "{} {} {}{}",
                            //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                            //     { "\x1b[38;5;92m XXX:\x1b[0m" },
                            //     { history.clone() },
                            //     { "?" }
                            // );
                            return 0;
                        }
                    } else {
                        // println!(
                        //     "{} {} {:?}",
                        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                        //     { "\x1b[38;5;92m XXX:\x1b[0m" },
                        //     { history.clone() }
                        // );
                        return 0;
                    }
                }
                Status::Unknown => {
                    if remain_damaged == 1 {
                        if cur_size == 1 {
                            // println!(
                            //     "{} {} {}{}",
                            //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                            //     { "\x1b[38;5;92m LLL:\x1b[0m" },
                            //     { history.clone() },
                            //     { "#" }
                            // );
                            return 1;
                        } else {
                            // println!(
                            //     "{} {} {}{}",
                            //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                            //     { "\x1b[38;5;92m XXX:\x1b[0m" },
                            //     { history.clone() },
                            //     { "?" }
                            // );
                            return 0;
                        }
                    } else if remain_damaged == 0 {
                        // println!(
                        //     "{} {} {}{}",
                        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                        //     { "\x1b[38;5;92m LLL:\x1b[0m" },
                        //     { history.clone() },
                        //     { "." }
                        // );
                        return 1;
                    } else {
                        // println!(
                        //     "{} {} {:?}",
                        //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                        //     { "\x1b[38;5;92m XXX:\x1b[0m" },
                        //     { history.clone() }
                        // );
                        return 0;
                    }
                }
            }
        }
        // too many damaged, spings, no possible
        // if remain_damaged == 0 {
        //     // println!(
        //     //     "{} {} {:?}",
        //     //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        //     //     { "\x1b[38;5;92m XXX:\x1b[0m" },
        //     //     { history.clone() }
        //     // );
        //     return 0;
        // }

        if matches!(cur_elt, Status::Damaged) && cur_size == 0 && !input.is_empty() {
            // println!(
            //     "{} {} {:?}",
            //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
            //     { "\x1b[38;5;92m XXX:\x1b[0m" },
            //     { history.clone() }
            // );
            return 0;
        }

        /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ Check cache ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
        let binding = cache.clone();
        let actual_cache = binding.borrow();
        if actual_cache.contains_key(&(prev_elt, cur_elt, cur_size, input, sizes, remain_damaged)) {
            let cached_val = *actual_cache
                .get(&(prev_elt, cur_elt, cur_size, input, sizes, remain_damaged))
                .unwrap();
            // println!(
            //     "{} {} {:?} -> {:?}",
            //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
            //     { "\x1b[38;5;92m DDD: in cache\x1b[0m" },
            //     { (prev_elt, cur_elt, cur_size, input, sizes) },
            //     { cached_val }
            // );
            return cached_val;
        }
        drop(actual_cache);

        match cur_elt {
            /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ Damaged ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
            Status::Damaged => {
                let new_cur_size: usize;
                let new_sizes: &[usize];
                match input[0] {
                    // finish group, go to the next
                    Status::Operational => {
                        if sizes.is_empty() {
                            new_sizes = sizes;
                            new_cur_size = cur_size - 1;
                        } else {
                            new_sizes = &sizes[1..];
                            new_cur_size = sizes[0];
                        }

                        // group ended early
                        if cur_size > 1 {
                            // println!(
                            //     "{} {} {:?}",
                            //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                            //     { "\x1b[38;5;92m XXX:\x1b[0m" },
                            //     { history.clone() }
                            // );
                            return 0;
                        }
                        self.cached_recur_combis(
                            cache.clone(),
                            cur_elt,
                            input[0],
                            new_cur_size,
                            &input[1..],
                            new_sizes,
                            remain_damaged - 1,
                            format!("{history}{cur_elt_str}"),
                        )
                    }
                    Status::Damaged => self.cached_recur_combis(
                        cache.clone(),
                        cur_elt,
                        input[0],
                        cur_size - 1,
                        &input[1..],
                        sizes,
                        remain_damaged - 1,
                        format!("{history}{cur_elt_str}"),
                    ),
                    Status::Unknown => {
                        if cur_size > 1 {
                            self.cached_recur_combis(
                                cache.clone(),
                                cur_elt,
                                input[0],
                                cur_size - 1,
                                &input[1..],
                                sizes,
                                remain_damaged - 1,
                                format!("{history}{cur_elt_str}"),
                            )
                        } else if sizes.is_empty() {
                            new_sizes = sizes;
                            new_cur_size = 0;
                            self.cached_recur_combis(
                                cache.clone(),
                                cur_elt,
                                Status::Operational,
                                new_cur_size,
                                &input[1..],
                                new_sizes,
                                remain_damaged - 1,
                                format!("{history}{cur_elt_str}"),
                            )
                        } else {
                            new_sizes = &sizes[1..];
                            new_cur_size = sizes[0];
                            self.cached_recur_combis(
                                cache.clone(),
                                cur_elt,
                                Status::Operational,
                                new_cur_size,
                                &input[1..],
                                new_sizes,
                                remain_damaged - 1,
                                format!("{history}{cur_elt_str}"),
                            )
                        }
                    }
                }
            }

            /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ Operational ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
            Status::Operational => self.cached_recur_combis(
                cache.clone(),
                cur_elt,
                input[0],
                cur_size,
                &input[1..],
                sizes,
                remain_damaged,
                format!("{history}{cur_elt_str}"),
            ),
            /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ Unknown ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
            Status::Unknown => {
                /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ Check next ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
                if cur_size == 1 {
                    match input[0] {
                        Status::Operational => match prev_elt {
                            // .?.  & 1 -> .#. and go to next size
                            Status::Operational => {
                                self.cached_recur_combis(
                                    cache.clone(),
                                    prev_elt,
                                    Status::Damaged,
                                    cur_size,
                                    input,
                                    sizes,
                                    remain_damaged,
                                    history.clone(),
                                ) + self.cached_recur_combis(
                                    cache.clone(),
                                    prev_elt,
                                    Status::Operational,
                                    cur_size,
                                    input,
                                    sizes,
                                    remain_damaged,
                                    history,
                                )
                            }
                            // #?.  & 1 -> #..
                            Status::Damaged => {
                                let new_cur_size: usize;
                                let new_sizes: &[usize];
                                if sizes.is_empty() {
                                    new_sizes = &[];
                                    new_cur_size = 0;
                                } else {
                                    new_sizes = &sizes[1..];
                                    new_cur_size = sizes[0];
                                }
                                self.cached_recur_combis(
                                    cache.clone(),
                                    prev_elt,
                                    Status::Damaged,
                                    cur_size,
                                    input,
                                    sizes,
                                    remain_damaged,
                                    history.clone(),
                                ) + self.cached_recur_combis(
                                    cache.clone(),
                                    prev_elt,
                                    Status::Operational,
                                    new_cur_size,
                                    input,
                                    new_sizes,
                                    remain_damaged,
                                    history,
                                )
                            }
                            Status::Unknown => {
                                panic!("previous elt should have been replaced already")
                            }
                        },
                        Status::Damaged => match prev_elt {
                            // .?#  & 1 -> ..#
                            Status::Operational => self.cached_recur_combis(
                                cache.clone(),
                                prev_elt,
                                Status::Operational,
                                cur_size,
                                input,
                                sizes,
                                remain_damaged,
                                history,
                            ),
                            // #?#  & 1 -> X
                            Status::Damaged => {
                                let new_cur_size: usize;
                                let new_sizes: &[usize];
                                if sizes.is_empty() {
                                    new_sizes = &[];
                                    new_cur_size = 0;
                                } else {
                                    new_sizes = &sizes[1..];
                                    new_cur_size = sizes[0];
                                }

                                self.cached_recur_combis(
                                    cache.clone(),
                                    prev_elt,
                                    Status::Operational,
                                    new_cur_size,
                                    input,
                                    new_sizes,
                                    remain_damaged,
                                    history,
                                )
                            }
                            Status::Unknown => {
                                panic!("previous elt should have been replaced already")
                            }
                        },
                        Status::Unknown => {
                            match prev_elt {
                                // .??  & 1 -> ..# or .#.
                                Status::Operational => {
                                    self.cached_recur_combis(
                                        cache.clone(),
                                        prev_elt,
                                        Status::Damaged,
                                        cur_size,
                                        input,
                                        sizes,
                                        remain_damaged,
                                        history.clone(),
                                    ) + self.cached_recur_combis(
                                        cache.clone(),
                                        prev_elt,
                                        Status::Operational,
                                        cur_size,
                                        input,
                                        sizes,
                                        remain_damaged,
                                        history,
                                    )
                                }
                                // #??  & 1 -> #.# or #..
                                Status::Damaged => {
                                    // let new_cur_size: usize;
                                    // let new_sizes: &[usize];
                                    // if sizes.is_empty() {
                                    //     new_sizes = &[];
                                    //     new_cur_size = 0;
                                    // } else {
                                    //     new_sizes = &sizes[1..];
                                    //     new_cur_size = sizes[0];
                                    // }

                                    self.cached_recur_combis(
                                        cache.clone(),
                                        prev_elt,
                                        Status::Damaged,
                                        cur_size,
                                        input,
                                        sizes,
                                        remain_damaged,
                                        history,
                                    )
                                }
                                Status::Unknown => {
                                    panic!("previous elt should have been replaced already")
                                }
                            }
                        }
                    }
                }
                /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ No damaged sping left ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
                else if cur_size == 0 {
                    match input[0] {
                        Status::Operational | Status::Unknown => self.cached_recur_combis(
                            cache.clone(),
                            prev_elt,
                            Status::Operational,
                            cur_size,
                            &input[1..],
                            sizes,
                            remain_damaged,
                            format!("{history}."),
                        ),
                        Status::Damaged => {
                            // println!(
                            //     "{} {} {:?}",
                            //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                            //     { "\x1b[38;5;92m XXX:\x1b[0m" },
                            //     { history.clone() }
                            // );
                            return 0;
                        }
                    }
                }
                /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ if bigger than ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
                else {
                    match input[0] {
                        // .?.  & 2+ -> ... and go to next size
                        Status::Operational => match prev_elt {
                            Status::Operational => self.cached_recur_combis(
                                cache.clone(),
                                prev_elt,
                                Status::Operational,
                                cur_size,
                                input,
                                sizes,
                                remain_damaged,
                                history,
                            ),
                            // #?.  & 2+ -> X
                            Status::Damaged => {
                                // println!(
                                //     "{} {} {:?}",
                                //     { "\x1b[38;5;92m➤➤➤\x1b[0m" },
                                //     { "\x1b[38;5;92m XXX:\x1b[0m" },
                                //     { history.clone() }
                                // );
                                return 0;
                            }
                            Status::Unknown => {
                                panic!("previous elt should have been replaced already")
                            }
                        },
                        Status::Damaged => match prev_elt {
                            // .?#  & 2+ -> ..#
                            Status::Operational => {
                                if sizes.is_empty() {
                                    if cur_size > 1 {
                                        self.cached_recur_combis(
                                            cache.clone(),
                                            prev_elt,
                                            Status::Damaged,
                                            cur_size,
                                            input,
                                            sizes,
                                            remain_damaged,
                                            history.clone(),
                                        ) + self.cached_recur_combis(
                                            cache.clone(),
                                            prev_elt,
                                            Status::Operational,
                                            cur_size,
                                            input,
                                            sizes,
                                            remain_damaged,
                                            history,
                                        )
                                    } else {
                                        self.cached_recur_combis(
                                            cache.clone(),
                                            prev_elt,
                                            Status::Operational,
                                            cur_size,
                                            input,
                                            sizes,
                                            remain_damaged,
                                            history,
                                        )
                                    }
                                } else {
                                    self.cached_recur_combis(
                                        cache.clone(),
                                        prev_elt,
                                        Status::Damaged,
                                        cur_size,
                                        input,
                                        sizes,
                                        remain_damaged,
                                        history.clone(),
                                    ) + self.cached_recur_combis(
                                        cache.clone(),
                                        prev_elt,
                                        Status::Operational,
                                        cur_size,
                                        input,
                                        sizes,
                                        remain_damaged,
                                        history,
                                    )
                                }
                            }
                            // #?#  & 2+
                            Status::Damaged => self.cached_recur_combis(
                                cache.clone(),
                                prev_elt,
                                Status::Damaged,
                                cur_size,
                                input,
                                sizes,
                                remain_damaged,
                                history,
                            ),
                            Status::Unknown => {
                                panic!("previous elt should have been replaced already")
                            }
                        },
                        Status::Unknown => {
                            match prev_elt {
                                // .??  & 2+ -> ..# or .#.
                                Status::Operational => {
                                    if sizes.is_empty() {
                                        self.cached_recur_combis(
                                            cache.clone(),
                                            prev_elt,
                                            Status::Damaged,
                                            cur_size,
                                            input,
                                            sizes,
                                            remain_damaged,
                                            history.clone(),
                                        ) + self.cached_recur_combis(
                                            cache.clone(),
                                            prev_elt,
                                            Status::Operational,
                                            cur_size,
                                            input,
                                            sizes,
                                            remain_damaged,
                                            history,
                                        )
                                    } else {
                                        self.cached_recur_combis(
                                            cache.clone(),
                                            prev_elt,
                                            Status::Damaged,
                                            cur_size,
                                            input,
                                            sizes,
                                            remain_damaged,
                                            history.clone(),
                                        ) + self.cached_recur_combis(
                                            cache.clone(),
                                            prev_elt,
                                            Status::Operational,
                                            cur_size,
                                            input,
                                            sizes,
                                            remain_damaged,
                                            history,
                                        )
                                    }
                                }
                                // #??  & 2+ -> ##? or #.?
                                Status::Damaged => self.cached_recur_combis(
                                    cache.clone(),
                                    prev_elt,
                                    Status::Damaged,
                                    cur_size,
                                    input,
                                    sizes,
                                    remain_damaged,
                                    history.clone(),
                                ),
                                Status::Unknown => {
                                    panic!("previous elt should have been replaced already")
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // fn damage_nb(&self) -> usize {
    //     self.statuses
    //         .iter()
    //         .filter(|x| matches!(**x, Status::Damaged))
    //         .count()
    // }

    fn unkown_nb(&self) -> usize {
        self.statuses
            .iter()
            .filter(|x| matches!(**x, Status::Unknown))
            .count()
    }
}

fn parser(input: &str) -> Vec<Entry> {
    let (_, entries) = parse_str(input).unwrap();

    entries
}

fn parse_status(input: &str) -> IResult<&str, Vec<Status>> {
    let (remain, res) = many1(alt((tag("."), tag("#"), tag("?"))))(input)?;

    let res: Vec<Status> = res
        .iter()
        .map(|x| match *x {
            "#" => Status::Damaged,
            "." => Status::Operational,
            "?" => Status::Unknown,
            _ => panic!(),
        })
        .collect();

    Ok((remain, res))
}

fn parse_arr(input: &str) -> IResult<&str, Vec<usize>> {
    let (remain, res) = separated_list1(tag(","), digit1)(input)?;

    Ok((
        remain,
        res.iter().map(|x| x.parse::<usize>().unwrap()).collect(),
    ))
}

fn parse_str(input: &str) -> IResult<&str, Vec<Entry>> {
    let (input, output) = many1(terminated(
        separated_pair(parse_status, space1, parse_arr),
        alt((recognize(eof), recognize(newline))),
    ))(input)?;

    let res = output
        .into_iter()
        .map(|(s, a)| Entry {
            statuses: s.into_iter().collect(),
            arrs: a,
            ..Default::default()
        })
        .collect();
    Ok((input, res))
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[rstest]
    #[case("???.### 1,1,3", "1")]
    #[case(".??..??...?##. 1,1,3", "16384")]
    #[case("?#?#?#?#?#?#?#? 1,3,1,6", "1")]
    #[case("????.#...#... 4,1,1", "16")]
    #[case("????.######..#####. 1,6,5", "2500")]
    #[case("?###???????? 3,2,1", "506250")]
    #[case(
        "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1",
        "525152"
    )]
    fn test_part_2(#[case] input: &str, #[case] output: &str) {
        let result = part2(input);
        assert_eq!(result, output.to_string());
    }
}

// #[case("?.??..??#.? 2,2", "1")]
//     #[case("????.??.?? 4,1,1", "4")]
//     #[case("????.??.?? 4,1", "4")]
//     #[case("????. 4", "1")]
//     #[case(".??.??. 1", "4")]
//     #[case("?....? 1", "2")]
//     #[case("#????#???????? 1,9", "4")]
//     #[case(".??...??##? 2,4", "2")]
